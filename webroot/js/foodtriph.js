/**
 * Modal - Add New Merchant
 */
$("#btnNewMerchant").click(function(e){
	e.preventDefault();
    $.ajax({
        url: "/merchants/add", 
        success: function(result){
        	$("#modalNewMerchant .modal-body").html(result);
    	}
	});
	
});

/**
 * Modal - Update Merchant Image
 */
$(".merchant-image").click(function(e){
	e.preventDefault();
	obj = $(this);
	uuid = obj.attr('uuid');
    $.ajax({
        url: "/merchants/ajxEditPhoto/" + uuid, 
        success: function(result){
            
        	$("#modalEditMerchantPhoto .modal-body").html(result);
    	}
	});
	
});

/**
 * Modal - Edit Merchant Details
 */						
$("#btnEditMerchant").click(function(e){
	e.preventDefault();
    $.ajax({
        url: "/merchants/edit", 
        success: function(result){
        	$("#modalEditMerchant .modal-body").html(result);
    	}
	});
	
});

/**
 * Modal - Add Menu
 */
//$(".btnNewMenu").click(function(e){
//	e.preventDefault();
//	obj = $(this);
//    $.ajax({
//        url: "/menus/add/"+obj.attr('merchant-uuid'), 
//        success: function(result){
//        	$("#modalNewMenu .modal-body").html(result);
//    	}
//	});
//});

/**
 * Modal - Edit Menu
 */
//$(".btnEditMenu").click(function(e){
//	e.preventDefault();
//	obj = $(this);
//    $.ajax({
//        url: "/menus/edit/"+obj.attr('menu-id') + "/" + obj.attr('merchant-uuid'), 
//        success: function(result){
//        	$("#modalEditMenu .modal-body").html(result);
//    	}
//	});
//});

/**
 * Modal - Add New Branch (Merchant Addresss)
 */
$(".btnNewBranch").click(function(e){
	e.preventDefault();
	obj = $(this);
	$.ajax({
	    url: "/merchantAddresses/add/"+obj.attr('merchant-uuid'), 
	    success: function(result){
	    	$("#modalNewBranch .modal-body").html(result);
		}
	});

});

/**
 * Modal - Edit Branch (Merchant Addresss)
 */
$(".btnEditMerchantAddress").click(function(e){
	e.preventDefault();
	obj = $(this);
	$.ajax({
	    url: "/merchantAddresses/edit/"+obj.attr('branch-id')+"/"+obj.attr('merchant-uuid'), 
	    success: function(result){
	    	$("#modalEditBranch .modal-body").html(result);
		}
	});

});

/**
 * Modal - Add New Menu Addons
 */
$(".btnNewMenuAddOn").click(function(e){
	e.preventDefault();
	obj = $(this);
	$.ajax({
	    url: "/menuAddOns/add/"+obj.attr('merchant-uuid'), 
	    success: function(result){
	    	$("#modalNewMenuAddOn .modal-body").html(result);
		}
	});

});

/**
 * Modal - Edit Menu Addons
 */
$(".btnEditMenuAddOn").click(function(e){
	e.preventDefault();
	obj = $(this);
	$.ajax({
	    url: "/menuAddOns/edit/"+obj.attr('menu-addon-id')+'/'+obj.attr('vendor-uuid'), 
	    success: function(result){
	    	$("#modalEditMenuAddOn .modal-body").html(result);
		}
	});

});