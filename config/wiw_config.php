<?php
return [
	//Establishment Type:
    'BusinessType' => [
        'MER' => 'Merchant',
    	'MLL' => 'Mall',
    	'CCC' => 'Credit Card Company'
    ],
    
    //Environtment
	//'env' => 'http://localhost:9090/',
	//'env' => 'https://foodtriph-api.herokuapp.com/',
	//'env' => 'https://admin.WhereIsWhere.com/',
	'env' => 'http://localhost:8888/',
        'emailDefault' => 'support@whereiswhere.com',
	'defaultSender' => 'jmbrothers@gmail.com',
	'emailInfo' => 'accounts@whereiswhere.com',
	'emailSupport' => 'accounts@whereiswhere.com',
	'systemEmail' => 'accounts@whereiswhere.com',
	'defaultWebsite' => 'http://www.whereiswhere.com',
	'defaultLoginUrl' => 'http://www.whereiswhere.com',
	'defaultAppName' => 'WhereIsWhere',
	'defaultTagLine' => 'Finding excitement in just 3 clicks',
	//api url
	'apiUrl' => 'https://api01.foodtri.ph/',
    
	//default image
	'defaultImg' => "https://placeholdit.imgix.net/~text?txtsize=75&txt={{click-me}}&w=600&h=450",//"img/logo_foodtriph.png",
	'defaultImgProfile' => 'http://via.placeholder.com/750x150',
	'defaultImgJson' => '{"name":"FoodTri.PH.png","ext":"png","mime":"image\/png","size":"128156","md5":"e984f0ea1cfa2bba0470284aab4c4531","dimensions":{"width":379,"height":377},"path":"img"}',
	
	//download url android
	'downloadUrl_Android' => 'https://www.foodtri.ph',
	'downloadUrl_iOS' => 'https://www.foodtri.ph',
	
	//curreny symbol
	'defaultCurrencySymbol' => 'SGD',
	'defaultCurrencySymbolShort' => 'S$',
	
	//Transaction status
	'TransacStatus' => [
		0 => 'Cancelled',
		1 => 'Pending',
		2 => 'Accepted',
		3 => 'Paid',
		4 => 'Preparing',
		5 => 'Delivered',
	],

	//Currency
	'Currency' => [
		['short_name' =>'SGD', 'long_name'=> 'Singapore Dollar', 'country'=>'Singapore'],
		['short_name' =>'PHP','long_name'=> 'Philippine Peso', 'country'=>'Philippines'],
	],	
	
	//Country Available
	'Country' => [
		'SG' => 'Singapore',
		'PH' => 'Philippines',
		'TH' => 'Thailand',
	],

	//Category/Tag
	'Category' => [
		'appetizer' => 'Appetizer',
		'meal' => 'Meal',
		'snack' => 'Snack',
		'dessert' => 'Dessert',
		'canned' => 'Canned',
		'party_pack' => 'Party Pack',
		'accessories' => 'Accessories',
		'salad' => 'Salad',
		'bread' => 'Bread',
	],	
];
