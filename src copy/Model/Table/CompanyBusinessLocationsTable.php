<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CompanyBusinessLocations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $CompanyBusinessCategories
 * @property \Cake\ORM\Association\BelongsTo $Malls
 * @property \Cake\ORM\Association\BelongsTo $Cccs
 * @property \Cake\ORM\Association\BelongsTo $Places
 *
 * @method \App\Model\Entity\CompanyBusinessLocation get($primaryKey, $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyBusinessLocation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompanyBusinessLocationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('company_business_locations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id'
        ]);
        $this->belongsTo('CompanyBusinessCategories', [
            'foreignKey' => 'company_business_category_id'
        ]);
        $this->belongsTo('Malls', [
            'foreignKey' => 'mall_id'
        ]);
        $this->belongsTo('Cccs', [
            'foreignKey' => 'ccc_id'
        ]);
        $this->belongsTo('Places', [
            'foreignKey' => 'place_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('uuid')
            ->add('uuid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('company_uuid');

        $validator
            ->allowEmpty('business_name');

        $validator
            ->allowEmpty('company_business_sub_categories');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('is_company_logo');

        $validator
            ->numeric('latitude')
            ->allowEmpty('latitude');

        $validator
            ->numeric('longitude')
            ->allowEmpty('longitude');

        $validator
            ->numeric('pin_latitude')
            ->allowEmpty('pin_latitude');

        $validator
            ->numeric('pin_longitude')
            ->allowEmpty('pin_longitude');

        $validator
            ->allowEmpty('address1');

        $validator
            ->allowEmpty('address2');

        $validator
            ->allowEmpty('street');

        $validator
            ->allowEmpty('city');

        $validator
            ->allowEmpty('state');

        $validator
            ->allowEmpty('country');

        $validator
            ->allowEmpty('country_code');

        $validator
            ->allowEmpty('postal_code');

        $validator
            ->allowEmpty('unit_num');

        $validator
            ->allowEmpty('landmarks');

        $validator
            ->allowEmpty('is_company_contact');

        $validator
            ->allowEmpty('contact_person');

        $validator
            ->allowEmpty('contact_email');

        $validator
            ->allowEmpty('contact_num');

        $validator
            ->allowEmpty('operating_hours');

        $validator
            ->allowEmpty('currency');

        $validator
            ->allowEmpty('status');

        $validator
            ->allowEmpty('deleted');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['uuid']));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['company_business_category_id'], 'CompanyBusinessCategories'));
        $rules->add($rules->existsIn(['mall_id'], 'Malls'));
        $rules->add($rules->existsIn(['ccc_id'], 'Cccs'));
        $rules->add($rules->existsIn(['place_id'], 'Places'));

        return $rules;
    }
}
