<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CampaignNearbyPromotions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\CampaignNearbyPromotion get($primaryKey, $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CampaignNearbyPromotion findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CampaignNearbyPromotionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('campaign_nearby_promotions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_uuid'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('company_uuid');

        $validator
            ->allowEmpty('promotion_name');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('business_groups');

        $validator
            ->allowEmpty('business_locations');

        $validator
            ->integer('launch_type')
            ->allowEmpty('launch_type');

        $validator
            ->requirePresence('launched', 'create')
            ->notEmpty('launched');

        $validator
            ->allowEmpty('promo_time');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->requirePresence('deleted', 'create')
            ->notEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_uuid'], 'Companies'));

        return $rules;
    }
}
