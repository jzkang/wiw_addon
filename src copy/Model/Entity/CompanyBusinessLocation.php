<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CompanyBusinessLocation Entity
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property string $company_uuid
 * @property string $business_name
 * @property int $company_business_category_id
 * @property string $company_business_sub_categories
 * @property int $mall_id
 * @property int $ccc_id
 * @property string $logo
 * @property string $is_company_logo
 * @property float $latitude
 * @property float $longitude
 * @property float $pin_latitude
 * @property float $pin_longitude
 * @property string $place_id
 * @property string $address1
 * @property string $address2
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $country_code
 * @property string $postal_code
 * @property string $unit_num
 * @property string $landmarks
 * @property string $is_company_contact
 * @property string $contact_person
 * @property string $contact_email
 * @property string $contact_num
 * @property string $operating_hours
 * @property string $currency
 * @property string $status
 * @property string $deleted
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $created_by
 * @property int $modified_by
 *
 * @property \App\Model\Entity\Company $company
 */
class CompanyBusinessLocation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
