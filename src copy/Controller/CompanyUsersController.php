<?php
namespace App\Controller;

use Cake\Event\Event;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;

use Cake\Network\Email\Email;

use Cake\Core\Configure;

/**
 * CompanyUsers Controller
 *
 * @property \App\Model\Table\CompanyUsersTable $CompanyUsers */
class CompanyUsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('forgotPassword');
    }

	function login(){

		$companyUser = $this->CompanyUsers->newEntity();
		$this->set('blnCaptcha', false);
		
	    if ($this->request->is('post')) {
			if(!empty($this->request->data['reCaptcha'])) {
		    	//reCaptcha - validation
		    	if ( ! $this->reCaptcha() ) {
		    		$this->set('blnCaptcha', true);
		    		$this->Flash->error(__('reCAPTCHA missing. Please try again.'));
		    		return $this->redirect($this->request->referer());
		    	}
			}

	    	$user = $this->Auth->identify();

			if ($user) {

	    		$this->Auth->setUser($user);

	    		if ($user['role'] === 'sa') // super-admin or sys-admin
	    			return $this->redirect(['controller'=>'companies','action'=>'index']);

	    		$companyUserDetails = $this->CompanyUsers->get($user['id']);

	    		//first login with temp pass
	    		if ( 0 == $companyUserDetails->verified && empty($companyUserDetails->pass_expiry) ) {
	    			$companyUsersTable = TableRegistry::get('CompanyUsers');
	    			$companyUserDetails->verified = 1;
	    			$companyUsersTable->save($companyUserDetails);
	    		}

	    		//check for pass_expiry
	    		if (empty($companyUserDetails->pass_expiry) || ( strtotime(date('Y-m-d')) > strtotime($companyUserDetails->pass_expiry) ) ) {
	    			$this->Flash->success(__('You login using temporary password. We need to update your password now!'));
	    			return $this->redirect(['controller'=>'company-users','action'=>'password', $user['company_uuid'] ]);
	    		}

	    		return $this->redirect(['controller'=>'companies','action'=>'view', $user['company_uuid'] ]);

	    	} else {
	    		$this->set('blnCaptcha', true);
	    		$this->Flash->error(__('Your email or password is incorrect. Please try again.'));
	    	}
	    }

	    $this->set(compact('companyUser'));
	    $this->set('_serialize', ['companyUser']);

		$this->render('login','login');
	}

	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $companyUsers = $this->paginate($this->CompanyUsers);

        $this->set(compact('companyUsers'));
        $this->set('_serialize', ['companyUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Company User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $companyUser = $this->CompanyUsers->get($id, [
            'contain' => []
        ]);

        $this->set('companyUser', $companyUser);
        $this->set('_serialize', ['companyUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $companyUser = $this->CompanyUsers->newEntity();
        if ($this->request->is('post')) {

        	if($this->request->data['photo']['size']) {
        		//img upload setup
        		$strFilename = $this->request->data['company_uuid'] . '_' . preg_replace('/\s+/', '_', $this->request->data['first_name'] . '_' . $this->request->data['last_name']);
        		$arrImg = $this->uploadImg(['filename'=>$strFilename]);
        		if ($arrImg) $this->request->data['photo'] = json_encode($arrImg);
        	}

            $companyUser = $this->CompanyUsers->patchEntity($companyUser, $this->request->data);

            if ($this->CompanyUsers->save($companyUser)) {

                $this->Flash->success(__('The company user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The company user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('companyUser'));
        $this->set('_serialize', ['companyUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $companyUser = $this->CompanyUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $companyUser = $this->CompanyUsers->patchEntity($companyUser, $this->request->data);
            if ($this->CompanyUsers->save($companyUser)) {
                $this->Flash->success(__('The company user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The company user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('companyUser'));
        $this->set('_serialize', ['companyUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Company User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $companyUser = $this->CompanyUsers->get($id);
        if ($this->CompanyUsers->delete($companyUser)) {
            $this->Flash->success(__('The company user has been deleted.'));
        } else {
            $this->Flash->error(__('The company user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function password($companyUuid) {
				
		$companyUser = $this->CompanyUsers->newEntity();

		if ( $this->request->is(['patch','post','put']) ) {
			$companyUserDetails = $this->CompanyUsers->get($this->Auth->user()['id']);

 			$companyUsersTable = TableRegistry::get('CompanyUsers');
 			$companyUserDetails->password = $this->request->data['password'];
 			$companyUserDetails->pass_expiry = date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s') . "+90 day"));;

			if ($companyUsersTable->save($companyUserDetails)) {
				$this->Flash->success(__('Your password was updated successfully.'));

				return $this->redirect(['controller'=>'companies','action'=>'view', $companyUuid ]);

			} else {
				$this->Flash->error(__('The company user could not be saved. Please, try again.'));
			}
		}

		$this->set( compact('companyUser') );
	}

	public function forgotPassword () {

		$companyUser = $this->CompanyUsers->newEntity();

		if ($this->request->is('post')) {

			extract($this->request->data);

			//reCaptcha - validation
			if ( ! $this->reCaptcha() ) {
				$this->Flash->error(__('reCAPTCHA missing. Please try again.'));
				return $this->redirect($this->request->referer());
			}

			$companyUsersTable = TableRegistry::get('CompanyUsers');
			$companyUser = $companyUsersTable->find()->where(['email'=>$email])->first();

			if ( !empty($companyUser) ) {
				$tempPass = $this->generatePassword();

				$companyUser->pass_expiry = null;
				$companyUser->password = $tempPass;

				if ( $companyUsersTable->save($companyUser) ) {
					//Send Welcome Email
					$email = new Email();
					$email->transport('mailjet');
					$email->template('passwordReset');
					$email->viewVars(['strTempPass'=>$tempPass, 'strSellerName'=>$companyUser->first_name, 'env'=>Configure::read('env'), 'defaultImgProfile'=> Configure::read('defaultImgProfile')]);
					$email->emailFormat('html');
					$email->from(Configure::read('emailDefault'), Configure::read('defaultAppName'));
					$email->to($companyUser->email);
					$email->subject(Configure::read('defaultAppName').' - Password Reset');
					//$email->send();
					//debug($email->send());die;

					if ( $email->send() )
						$this->Flash->success(__('Please check your email ('.$companyUser->email.') for your temporary password'));

					return $this->redirect(['controller'=>'company-users', 'action' => 'login']);

				} else {

					$this->Flash->error(__('There is an internal error. We apologised. We will fix this the soonest possible'));

					return $this->redirect($this->request->referer());

				}




			} else {

				$this->Flash->error(__('We could not find your email. Please try again.'));

				return $this->redirect($this->request->referer());
			}

		}

		$this->set(compact('companyUser'));
		$this->set('_serialize', ['companyUser']);
		$this->render('forgotPassword','login');

	}
}
