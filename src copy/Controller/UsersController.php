<?php
namespace App\Controller;

use Cake\Event\Event;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;

use Cake\Network\Email\Email;

use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['interest','verify']);
    }
	
	function interest(){
		
		$this->render('message_board');

		$User = $this->Users->newEntity();
		
		if ($this->request->is('post')) {
		
			$this->request->data('password','foodtriPH');
			$this->request->data('uuid', uniqid());
			
			$User = $this->Users->patchEntity($User, $this->request->data);
		
			if ($arrUser = $this->Users->save($User)) {

				$this->Flash->success(__('Thank you! We will notify you when <strong>foodtri</strong>PH is ready for download.'), ['escape' => false]);
				
			} else {
				
				$this->Flash->error(__('Your email already exist. Please share <strong>foodtri</strong>PH with your friends instead. :) '), ['escape' => false]);
				
			}
		}
		
		$this->render('message_board');	
		
	}	
	
	public function verify() {
		//debug($this->request->params['pass']);

		$data['email'] = base64_decode($this->request->params['pass'][0]);
		$data['hash'] = $this->request->params['pass'][1];		
		$user = $this->Users->find()->select(['id','email','verified'])->where(['email' => $data['email']])->first();
		
		if (empty($this->request->params['pass'])) {
			$this->Flash->error(__('Email Verification Error!'));
		} elseif (empty($user)) {
			//check email if exist
			$this->Flash->error(__('Email Verification Error! #0001'));	
		} elseif ( (int)$user['verified'] ) {
			//if user is already verified
			$this->Flash->info(__('Your email is already verified.'));
		} elseif ( md5($data['email']) != $data['hash']) {
			//if the md5 of email is NOT equal to hash code return error			
			$this->Flash->error(__('Email Verification Error! #0002'));
		} else {
			$data['verification_details'] = json_encode([
					'date_time' => date('c'),
					'user_agent' => $_SERVER['HTTP_USER_AGENT'],
					'ip_address' => $_SERVER['REMOTE_ADDR'],
					'query_string' => $_SERVER['QUERY_STRING']
					]);
			$data['verified'] = 1;
			
			$user = $this->Users->patchEntity($user, $data);
			
			if ($this->Users->save($user))
				$this->Flash->success(__('Your email is now verified. Please re-login from <strong>foodtri</strong>PH app to sync your data.'), ['escape' => false]);
			else
				$this->Flash->error(__('Email Verification Error! #0003'));			
		}
		
		$this->render('message_board');
	}
	

}
