<?php
namespace App\Controller;

use Cake\Utility\Inflector;

use Cake\Controller\Component\AuthComponent;

use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use App\Controller\AppController;

use Cake\Utility\Hash;

use Cake\Network\Email\Email;

use Cake\Event\Event;

/**
 * CompanyBusinessGroups Controller
 *
 * @property \App\Model\Table\CompanyBusinessGroupsTable $CompanyBusinessGroups
 */
class CompanyBusinessGroupsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($companyUuid)
    {
        $companyBusinessGroups = $this->CompanyBusinessGroups->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);

        $address=array();
        $list=array();
        $companyBusinessLocationTable = TableRegistry::get('CompanyBusinessLocations');
        $companyBusinessLocations = $companyBusinessLocationTable->find()->where(['company_uuid'=>$companyUuid]);
        
        foreach ($companyBusinessLocations as $loc){
            $list[trim($loc['uuid'])]=$loc['business_name']." : ".(trim($loc["address1"])!=="" ? $loc["address1"] : "")
                                                                    .(trim($loc["address2"])!=="" ? ", ".$loc["address2"] : "")
                                                                    .(trim($loc["street"])!=="" ? ", ".$loc["street"] : "")
                                                                    .(trim($loc["state"])!=="" ? ", ".$loc["state"] : "")
                                                                    .(trim($loc["city"])!=="" ? ", ".$loc["city"] : "")
                                                                    .(trim($loc["postal_code"])!=="" ? ", ".$loc["postal_code"] : "");
        }
//debug($list);

        $this->set(compact('companyBusinessGroups','companyUuid','companyBusinessLocations','list'));
        $this->set('_serialize', ['companyBusinessGroups','companyUuid','companyBusinessLocations','list']);
    }

    /**
     * View method
     *
     * @param string|null $id Company Business Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $companyBusinessGroup = $this->CompanyBusinessGroups->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('companyBusinessGroup', $companyBusinessGroup);
        $this->set('_serialize', ['companyBusinessGroup']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($companyUuid)
    {
        $action = "add";
        $user = $this->user;

//die(debug($this->request->data));

        $companyBusinessGroup = $this->CompanyBusinessGroups->newEntity();
        if ($this->request->is('post')) {
            
            $companyBusinessGroup->company_uuid = $this->request->data['companyUuid'];
            $companyBusinessGroup->group_name = $this->request->data['group_name'];
            $companyBusinessGroup->description = $this->request->data['description'];
            $companyBusinessGroup->business_location = json_encode($this->request->data['bu']);

            if ($this->CompanyBusinessGroups->save($companyBusinessGroup)) {
                $this->Flash->success(__('The new company business group has been saved.'));

                return $this->redirect(['action' => 'index', $companyUuid]);
            }
            $this->Flash->error(__('The company business group could not be saved. Please, try again.'));
        }
        
        $list=array();
        $companyBusinessLocationTable = TableRegistry::get('CompanyBusinessLocations');
        $companyBusinessLocations = $companyBusinessLocationTable->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);
        
        foreach ($companyBusinessLocations as $loc){
            $list[trim($loc['uuid'])]=$loc['business_name']." : ".(trim($loc["address1"])!=="" ? $loc["address1"] : "")
                                                                    .(trim($loc["address2"])!=="" ? ", ".$loc["address2"] : "")
                                                                    .(trim($loc["street"])!=="" ? ", ".$loc["street"] : "")
                                                                    .(trim($loc["state"])!=="" ? ", ".$loc["state"] : "")
                                                                    .(trim($loc["city"])!=="" ? ", ".$loc["city"] : "")
                                                                    .(trim($loc["postal_code"])!=="" ? ", ".$loc["postal_code"] : "");
        }

        //debug($companyBusinessLocations);

        $this->set(compact('companyUuid','companyBusinessGroup','action','list','user'));
        $this->set('_serialize', ['companyBusinessGroup']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company Business Group id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        return $this->redirect(['action' => 'index', $companyUuid]);
        /*
        $companyBusinessGroup = $this->CompanyBusinessGroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $companyBusinessGroup = $this->CompanyBusinessGroups->patchEntity($companyBusinessGroup, $this->request->data);
            if ($this->CompanyBusinessGroups->save($companyBusinessGroup)) {
                $this->Flash->success(__('The company business group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company business group could not be saved. Please, try again.'));
        }
        $companies = $this->CompanyBusinessGroups->Companies->find('list', ['limit' => 200]);
        $this->set(compact('companyBusinessGroup', 'companies','companyUuid'));
        $this->set('_serialize', ['companyBusinessGroup']);
        */
    }

    /**
     * Delete method
     *
     * @param string|null $id Company Business Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null,$companyUuid)
    {
        
        $this->request->allowMethod(['post', 'delete']);
        $companyBusinessGroup = $this->CompanyBusinessGroups->get($id, [
            'contain' => []
        ]);
//die(debug($companyBusinessGroup));
        $companyBusinessGroup->deleted = 1;

        if ($this->CompanyBusinessGroups->save($companyBusinessGroup)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index', $companyUuid]);

        /*
        $this->request->allowMethod(['post', 'delete']);
        $companyBusinessGroup = $this->CompanyBusinessGroups->get($id);
        if ($this->CompanyBusinessGroups->delete($companyBusinessGroup)) {
            $this->Flash->success(__('The company business group has been deleted.'));
        } else {
            $this->Flash->error(__('The company business group could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
        */
    }

    public function editBusinessGroup($id = null, $companyUuid)
    {
       
        $action = "editBusinessGroup";
        $companyBusinessGroup = $this->CompanyBusinessGroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $companyBusinessGroup = $this->CompanyBusinessGroups->patchEntity($companyBusinessGroup, $this->request->data);

            $companyBusinessGroup->business_location = json_encode($this->request->data['bu']);

            if ($this->CompanyBusinessGroups->save($companyBusinessGroup)) {
                $this->Flash->success(__('The company business group has been saved.'));

                return $this->redirect(['action' => 'index', $companyUuid]);
            }
            $this->Flash->error(__('The company business group could not be saved. Please, try again.'));
        }

//debug($companyBusinessGroup);
        $companyBusinessGroup = $this->CompanyBusinessGroups->find()->where(['id'=>$id,'company_uuid'=>$companyUuid,'deleted'=>'0'])->toArray();

        $address=array();
        $list=array();
        $companyBusinessLocationTable = TableRegistry::get('CompanyBusinessLocations');
        $companyBusinessLocations = $companyBusinessLocationTable->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);
        
        foreach ($companyBusinessLocations as $loc){
            $list[trim($loc['uuid'])]=$loc['business_name']." : ".(trim($loc["address1"])!=="" ? $loc["address1"] : "")
                                                                    .(trim($loc["address2"])!=="" ? ", ".$loc["address2"] : "")
                                                                    .(trim($loc["street"])!=="" ? ", ".$loc["street"] : "")
                                                                    .(trim($loc["state"])!=="" ? ", ".$loc["state"] : "")
                                                                    .(trim($loc["city"])!=="" ? ", ".$loc["city"] : "")
                                                                    .(trim($loc["postal_code"])!=="" ? ", ".$loc["postal_code"] : "");
        }

        //debug($companyBusinessLocations);

        $this->set(compact('companyUuid','companyBusinessGroup','action','list'));
        $this->set('_serialize', ['companyBusinessGroup']);
    }
}
