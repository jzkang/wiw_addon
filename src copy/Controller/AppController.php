<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Core\Configure;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Network\Http\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        //$this->loadComponent('Csrf');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash',['clear' => true]);
        $this->loadComponent('Auth', [
        	'authorize' => ['Controller'],
        	'loginAction' => [
        		'controller' => 'CompanyUsers',
        		'action' => 'login'
        	],       
            'loginRedirect' => [
                'controller' => 'Companies',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'CompanyUsers',
                'action' => 'login',
                'home'
            ],
        	'authError' => 'Unauthorized Access',
        	'authenticate' => [
        		'Form' => [
        			'fields' => [
        				'username' => 'email',
        				'password' => 'password'
        				],
        			'userModel'=>'CompanyUsers'
        		]
        	],    
        	'storage' => 'Session',
        	
        ]);

        //Configs to make it available in Views
        $this->set('arrFoodType', Configure::read('FoodType'));
        $this->set('arrTransacStatus', Configure::read('TransacStatus'));
        $this->set('apiUrl', Configure::read('apiUrl'));
        $this->set('env', Configure::read('env'));
        $this->set('emailDefault', Configure::read('emailDefault'));
        $this->set('emailInfo', Configure::read('emailInfo'));
        $this->set('emailSupport', Configure::read('emailSupport'));
        $this->set('downloadUrl_Android', Configure::read('downloadUrl_Android'));
        $this->set('downloadUrl_iOS', Configure::read('downloadUrl_iOS'));
        $this->set('systemEmail', Configure::read('systemEmail'));
        $this->set('defaultImg', Configure::read('defaultImg'));
        $this->set('defaultImgProfile', Configure::read('defaultImgProfile'));
        $this->set('defaultImgJson', Configure::read('defaultImgJson'));
        $this->set('defaultCurrencySymbol', Configure::read('defaultCurrencySymbol'));
        $this->set('defaultCurrencySymbolShort', Configure::read('defaultCurrencySymbolShort'));
        $this->set('FoodType', Configure::read('FoodType'));
        $this->set('Currency', Configure::read('Currency'));
        $this->set('Category', Configure::read('Category'));
        $this->set('Country', Configure::read('Country'));
        $this->set('defaultWebsite', Configure::read('defaultWebsite'));
        $this->set('defaultAppName', Configure::read('defaultAppName'));
        $this->set('defaultTagLine', Configure::read('defaultTagLine'));
        $this->set('defaultLoginUrl', Configure::read('defaultLoginUrl'));
        $this->set('BusinessType', Configure::read('BusinessType'));
        
        date_default_timezone_set('Asia/Singapore');
        

    }

    public function isAuthorized ($user) {
    	
    	//Super Admin (sa) users can access all
    	//if ( isset($user['role']) && $user['role'] === 'sa' ) return true;
    	$this->set('user', $user);
    	return true;
    	
    }
    
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    
    public function beforeFilter(Event $event)
    {
    	
    	//$this->Auth->allow(['index', 'view', 'display']);
    	$this->Auth->allow(['controller'=>'companies', 'action'=> 'add']);
    	//$this->Auth->allow(['controller'=>'companies', 'action'=> 'verifyLocation']);
    	//$this->Auth->allow(['controller'=>'company-users', 'action'=> 'forgotPassword']);
    }    

    /**
     * @method uploadImg - upload image
     * @param array $arrOption
     * - $arrOption['filename']
     * @return multitype:string NULL |boolean
     */
    public function uploadImg($arrOption = []) {
    	
    	$imgPath = Configure::read('App.imageBaseUrl') . Inflector::underscore($this->name);
    	$storage = new \Upload\Storage\FileSystem($imgPath, true);
    	$file = new \Upload\File('photo', $storage);
    	
    	
    	// Optionally you can rename the file on upload
    	if ($arrOption['filename']) 
    		$file->setName($arrOption['filename']);
    	
    	// Validate file upload
    	// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
    	$file->addValidations(array(
    			// Ensure file is of type "image/png"
    			//new \Upload\Validation\Mimetype('image/png'),
    	
    			//You can also add multi mimetype validation
    			new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpg', 'image/jpeg')),
    	
    			// Ensure file is no larger than 5M (use "B", "K", M", or "G")
    			new \Upload\Validation\Size('5M')
    	));
    	
    	// Access data about the file that has been uploaded
    	$data = array(
    			'name'       => $file->getNameWithExtension(),
    			'ext'  		 => $file->getExtension(),
    			'mime'       => $file->getMimetype(),
    			'size'       => $file->getSize()."",
    			'md5'        => $file->getMd5(),
    			'dimensions' => $file->getDimensions(),
    			'path'		 => $imgPath
    	);   

//     	$filePath = $imgPath . DS . $arrOption['filename'] . "." . $file->getExtension();
//     	if(file_exists($filePath))
//     		unlink($filePath);
    	
    	if($file->upload()) return $data;     	
    	
    	return false;
    	
    }

    public function uploadFile($arrOption = []) {

    	if (empty($arrOption['fieldName']))
    		$arrOption['fieldName'] = 'file';
    	
    	$strPath = 'files/' . Inflector::underscore($this->name);
    	$storage = new \Upload\Storage\FileSystem($strPath, true);
    	$file = new \Upload\File($arrOption['fieldName'], $storage);

    	// Optionally you can rename the file on upload
    	if (!empty($arrOption['filename']))
    		$file->setName($arrOption['filename']);
    	 
    	// Validate file upload
    	// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
    	$file->addValidations(array(
    	// Ensure file is of type "image/png"
    	//new \Upload\Validation\Mimetype('image/png'),
    	 
    	//You can also add multi mimetype validation
    	new \Upload\Validation\Mimetype(array('application/pdf')),
    	 
    	// Ensure file is no larger than 5M (use "B", "K", M", or "G")
    	new \Upload\Validation\Size('5M')
    	));
    	 
    	// Access data about the file that has been uploaded
    	$data = array(
    			'name'       => $file->getNameWithExtension(),
    			'ext'  		 => $file->getExtension(),
    			'mime'       => $file->getMimetype(),
    			'size'       => $file->getSize()."",
    			'md5'        => $file->getMd5(),
    			//'dimensions' => $file->getDimensions(),
    			'path'		 => $strPath
    	);
    
    	//     	$filePath = $imgPath . DS . $arrOption['filename'] . "." . $file->getExtension();
    	//     	if(file_exists($filePath))
    		//     		unlink($filePath);
    		 
    		if($file->upload()) return $data;
    		 
    		return false;
    		 
    }    
    
    /**
     * @method getMenuAddOns - retrieve all the menu add-ons of the company
     * @param int company_id
     * @return multitype:array | boolean
     */
	public function getMenuAddOns ($companyUuid) {
		$arrMenuAddOns = null;
		$menuAddOns = TableRegistry::get('MenuAddOns');
		$menuAddOns = $menuAddOns->find('all')
							->select(['id','parent_id','ref','add_on_name','price','description'])
							->where(['MenuAddOns.company_uuid'=> $companyUuid, 'MenuAddOns.active'=>1,'MenuAddOns.deleted'=>0])
							->order(['parent_id ASC'])->all();

		foreach ($menuAddOns as $v){

			if ( 0 === $v->parent_id ) {
				//$arrMenuAddOns[$v->id] = [];
				$arrMenuAddOns[$v->id] = ['id'=>$v->id, 'category_name'=>$v->add_on_name, 'description' => $v->description];
				continue;
			}
			
			$arrMenuAddOns[$v->parent_id]['items'][] = $v;

		}		
		
		if ($arrMenuAddOns) return $arrMenuAddOns;
		
		return false;
	}
	
	/**
	 * @method getLatLang - get the latitude and longitude of the given address
	 * @param object $companyAddress
	 * @return object
	 */	
	public function getLatLong (&$companyAddress) {
		//Get lat/long
		//https://maps.googleapis.com/maps/api/geocode/json?address=122E%20Rivervale%20Drive,%20Sengkang%20Singapore
		$strAddress = urlencode("$companyAddress->address2 $companyAddress->street $companyAddress->city $companyAddress->state $companyAddress->country $companyAddress->post_code") ;
		$arrLoc = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . $strAddress);
		$arrLoc = json_decode($arrLoc, true);
		 
		if (!empty($arrLoc['results'])) {
			$companyAddress->latitude = $arrLoc['results'][0]['geometry']['location']['lat'];
			$companyAddress->longitude = $arrLoc['results'][0]['geometry']['location']['lng'];
			$companyAddress->place_id = $arrLoc['results'][0]['place_id'];
		} else {
			$companyAddress->lat = 0;
			$companyAddress->long = 0;
			$companyAddress->place_id = '';
		}
	
		return $companyAddress;
	}	
	
	/**
	 * @method getLocationCurrency - get the currency of the users location base on latlong
	 * @param string $latlong
	 * @return array
	 */	
	public function getLocationCurrency ($latlong) {
		$geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latlong.'&sensor=false' );
		$geocode = json_decode($geocode, true);
		if (!empty($geocode)) {
			foreach (Hash::extract($geocode, 'results.0.address_components.{n}') as $v ) {
				if ($v['types'][0] === 'country') {
					$country = $v['long_name'];break;
				}
			}
		
			$arrCurrency = Hash::extract( Configure::read('Currency') , '{n}[country=/'.$country.'/]' );
			if (!empty($arrCurrency)) return $arrCurrency; 
		}		
		
		return false;
	}
	
	/**
	 * @method getCountry - return the country of operation
	 * @param string $latlong
	 * @return array
	 */	
	public function getLocationCountry($latlong) {
		$geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latlong.'&sensor=false' );
		$geocode = json_decode($geocode, true);
		if (!empty($geocode)) {
			foreach (Hash::extract($geocode, 'results.0.address_components.{n}') as $v ) {
				if ($v['types'][0] === 'country') {
					$country = $v['long_name'];break;
				}
			}
		
			if(!empty($country)) return $country;
		}
		
		return false;		 
	}	
	
	/**
	 * @method generatePassword - generate new password
	 * @return string
	 */
	public function generatePassword () {
			
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
	
		return implode($pass); //turn the array into a string
	
	}	

	/**
	 * @method generatePassword - generate new password
	 * @return string
	 */
	public function reCaptcha () {
		
		if (empty($this->request->data['g-recaptcha-response']))
			return false;
		
		$http = new Client();
		$jsonPayload = [ 'secret' => '6Ld9RRgUAAAAAEuuKnl1flsVVR3eWQkPJGTB0wd2','response'=>$this->request->data['g-recaptcha-response'], 'remoteip' => $_SERVER['REMOTE_ADDR'] ];
		$response = $http->post('https://www.google.com/recaptcha/api/siteverify', $jsonPayload, ['type' => 'json']);
		if (!$response->json['success']) 
			return false;
		
		return true;
	}

	/**
	 * @method getCategories - return all the categories from the menu
	 * @return string
	 */	
	public function getCategories ($companyUuid = null) {
		//Get the Categories
		$menu = TableRegistry::get('Menus');
		$Category = $this->Menus->find('all')->select(['menu_category_name'])->distinct(['menu_category_name'])->toArray();
		if (!empty($Category)) {
			$arrCategory = null;
			foreach (Hash::extract($Category, '{n}.menu_category_name') as $v) {
				if (empty($v)) continue;
				$arrCategory[Inflector::slug(strtolower($v), '_')] = ucwords($v);
			}
			$arrCategory = array_merge($arrCategory, Configure::read('Category') );
			$this->set('Category', $arrCategory);
			
			return $arrCategory;
		}

		return Configure::read('Category');
	}

}
