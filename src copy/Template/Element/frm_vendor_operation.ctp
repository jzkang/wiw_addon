<?php 
use Cake\Utility\Hash;

if (!empty($vendorAddress->operating_hours)) {
	$arrOperation = json_decode($vendorAddress->operating_hours, true);
	
	foreach ( $arrOperation as $k => $v ) {
		
		if (!empty($v['freetext'])) {
			$arrFreeText[] = $v['freetext']; 
			continue;
		}
		extract($v);
	} 
} 

$arrOperation = [
	['label'=>'Monday','alias'=>'mon'],
	['label'=>'Tuesday','alias'=>'tue'],
	['label'=>'Wednesday','alias'=>'wed'],
	['label'=>'Thursday','alias'=>'thu'],
	['label'=>'Friday','alias'=>'fri'],
	['label'=>'Staturday','alias'=>'sat'],
	['label'=>'Sunday','alias'=>'sun'],
];
$counter = 0;
if (empty($action)) $action = 'add';

?>

<div class="box box-primary">
	<div class="box-header with-border <?=(@$missingMeetup) ? 'bg-red' : ''?>">
		<label>
			<h3 class="profile-username">Meetup Timing and Location</h3>
		</label>
	</div>
	<div class="box-body box-profile">
		<?= $this->Form->create($vendorAddress,['class'=>"form-horizontal",'url'=>['controller'=>'vendorAddresses','action'=>$action ,$vendor->uuid]]) ?>
	    <?php 
	    	//Hidden Fields
	    	echo $this->Form->hidden('id',['value'=>$vendorAddress->id]);
	    	echo $this->Form->hidden('vendor_id',['value'=>$vendor->id]);
	    	echo $this->Form->hidden('vendor_uuid',['value'=>$vendor->uuid]);
	    ?>
	    
	    <div class="row">
	    	<div class="col-lg-12">
	    		<span><strong>Meet anytime / anywhere</strong></span><small>&nbsp;(Use this field for specific meetup setup)</small>
			    <!-- Free text if no Specific timing -->
			    <?php if (!empty($arrFreeText)):?>
			    <?php foreach ($arrFreeText as $v):?>
				<?= $this->Form->input("op[freetext][]", ['label'=>false, 'type'=>'text', 'value'=>$v, 'class'=>'form-control','placeholder'=>"e.g. Pickup only at my store around 7pm onwards on weekdays"]);?>
				<?php endforeach;?>
				<?php else:?>
				<?= $this->Form->input("op[freetext][]", ['label'=>false, 'type'=>'text', 'value'=>@$freetext, 'class'=>'form-control','placeholder'=>"e.g. Pickup only at my store around 7pm onwards on weekdays"]);?>
				<?php endif;?>
				<div class="input text opFreeTextField"></div>
				<div class="box-footer clearfix no-border" id="opFreeTextField">
				
					<?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right']) ?>&nbsp;&nbsp;&nbsp;
              		<button type="button" copy-fieldId="op-freetext" class="btn btn-default pull-right btnAddMoreField"><i class="fa fa-plus"></i> Add More</button>
              		
              		
					<script>
					/* Add Field */
					$(".btnAddMoreField").click(function(e){
						e.preventDefault();
						$('#op-freetext').clone().appendTo(".opFreeTextField");
					});
					</script>              		
            	</div>				
				<br>	    	
	    	</div>
	    </div>
		
<?php foreach ($arrOperation as $value):$counter++;?>
<?php 
$start = (empty(${$value['alias']}['start'])) ? "10:00 AM" : ${$value['alias']}['start'];
$end = (empty(${$value['alias']}['start'])) ? "10:00 AM" : ${$value['alias']}['end'];
//box box-success 
?>
<?php ob_start(); ?>
		<div class="col-lg-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">
						<label>
							<input type="hidden" class="minimal" name="op[<?=$value['alias']?>]" value="<?=$value['alias']?>"> 
							<?= $value['label']?>
						</label>
					</h3>
				</div>
				<div class="box-body">
					
						<div class="col-xs-4"><span>From</span></div>
						<div class="col-xs-8">
							<div class="bootstrap-timepicker">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control timepicker" name="op[<?=$value['alias']?>][start]" value="<?=$start ?>">
										<div class="input-group-addon">
				                      		<i class="fa fa-clock-o"></i>
				                    	</div>
				                  	</div>
								</div>
							</div>				
						</div>
							
						<div class="col-xs-4"><span>To</span></div>
						<div class="col-xs-8">
							<div class="bootstrap-timepicker">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control timepicker" name="op[<?=$value['alias']?>][end]" value="<?=$end ?>">
										<div class="input-group-addon">
				                      		<i class="fa fa-clock-o"></i>
				                    	</div>
				                  	</div>
								</div>
							</div>				
						</div>
						
						<div class="col-xs-4"><span>Location</span></div>
						<div class="col-xs-8" style="padding:0px;">
							<?= $this->Form->input("op[$value[alias]][loc]", ['label'=>false, 'type'=>'text', 'value'=>@${$value['alias']}['loc'], 'class'=>'form-control','placeholder'=>'e.g. Sengkang MRT Stn']);?>
							<p class="help-block">Empty means no meet-up for <?= $value['label']?></p>
						</div>					
				</div>
			</div>
		</div>
	
<?php $strHtml = ob_get_clean()?>
<?php
	if($counter === 1) echo '<div class="row">';
		
	
	echo $strHtml;
		
	if (!($counter % 2)) echo '</div><div class="row">';

?>	
<?php endforeach;echo '</div>';?>
<script type="text/javascript">$(".timepicker").timepicker({showInputs: false,time: 10:30 pm});</script>

              <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <?php if($this->request->isAjax):?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php endif;?>                
                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right']) ?>
                <?= $this->Form->end() ?>
              </div>

	</div> <!-- end of box-profile-->
</div>	<!-- end of box-primary -->
