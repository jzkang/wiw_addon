<?php
use Cake\Utility\Hash;
 
$arrPhoto = json_decode($vendor->photo,true);
$imgPathVendor = $defaultImg;
if ( !empty($arrPhoto) ) $imgPathVendor = $this->request->webroot . $arrPhoto['path']. DS .$arrPhoto['name'];

//status display
$strStatusActive = '';
$strStatusInActive = 'active';
$strStatus = '<font class="text-red">(Closed)</font>';
if ($vendor->status) {
	$strStatusActive = 'active';
	$strStatusInActive = '';
	$strStatus = '<font class="text-green">(Open)</font>';
}

?>

<!-- Profile Image -->
<div class="box box-primary">
	<div class="box-body box-profile">
		<a>
			<img alt="Vendor Image" src="<?=$imgPathVendor?>" class='img-responsive'>
		</a>
		<ul class="list-group list-group-unbordered">
			<li class="list-group-item">
				<p><?= h($vendor->description) ?></p>
			</li>		
        	<li class="list-group-item">
            	<strong><i class="fa fa fa-calendar  margin-r-5"></i> <b>Seller Since</b></strong> <a class="pull-right"><?= date('d-M-Y',strtotime($vendor->created)); ?></a>
			</li>
			<?php /* 
			<li class="list-group-item">
				<strong><i class="fa fa fa-envelope margin-r-5"></i> <b>Email</b></strong> <a class="pull-right " href="mailto:<?=$vendor->email;?>" ><?= h($vendor->email) ?></a>
			</li>
			<li class="list-group-item">
				<strong><i class="fa fa fa-phone margin-r-5"></i> <b>Phone</b></strong> <a class="pull-right"><?= h($vendor->contact_num) ?></a>
			</li>
			*/ ?>
			<li class="list-group-item">
				<strong><i class="fa fa fa-money margin-r-5"></i> <b>Accept only </b></strong> <a class="pull-right"><?= h(Hash::extract($Currency,'{n}[short_name=/'.$vendor->currency.'/]')[0]['long_name']) ?></a>
			</li>
		</ul>
		<a href="https://www.foodtriph.com" target="_blank" class="btn btn-primary btn-block"><b>Order Now</b></a>
	</div>
	<!-- /.box-body -->
</div>
<!-- /.box -->
<?= $this->element('modals',  ['id'=>'modalEditVendor','modalTitle'=>'Edit Vendor'])?>
<?= $this->element('modals',  ['id'=>'modalEditVendorPhoto','modalTitle'=>'Update Photo'])?>
