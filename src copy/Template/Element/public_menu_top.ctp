

<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>FT</b>.PH</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>foodtri</b>PH</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top hidden-xs">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu ">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=$defaultImgProfile?>" class="user-image" alt="User Image">
              <span class="" href="https://www.foodtri.ph" target="_blank">Download Now</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>

<?= $this->element('modals',  ['id'=>'modalNewVendor','modalTitle'=>'New Vendor'])?>  