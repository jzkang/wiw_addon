<?php
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Core\Configure;
 
$arrFile = json_decode($company->company_letter,true);
if ( !empty($arrFile) ) $filePath = $this->request->webroot . $arrFile['path']. DS .$arrFile['name'];

//status display
$strStatusActive = '';
$strStatusInActive = 'active';
$strStatus = '<font class="text-red">(Closed)</font>';
if ($company->status) {
	$strStatusActive = 'active';
	$strStatusInActive = '';
	$strStatus = '<font class="text-green">(Open)</font>';
}

$strPublicUrl =  $this->request->webroot . "provider/".urlencode($company->name)."-".$company->uuid;
?>

<!-- Profile Image -->
<div class="box box-primary">
	<div class="box-header with-border">
		<i class="fa fa-info"></i>
		<h3 class="box-title"><?=__('Other Details')?></h3>
    </div>
	<div class="box-body box-profile row">

		<div class="col-md-12"> 
			<ul class="list-group list-group-unbordered">
	        	<li class="list-group-item">
	            	<strong>
	            		<i class="fa fa fa-barcode  margin-r-5"></i> 
	            		<b><?= __('Invitation Code') ?></b>
	            	</strong> 
	            	<a class="pull-right"><?= $company->invite_code; ?></a>
				</li>			
	        	<li class="list-group-item">
	            	<strong>
	            		<i class="fa fa  fa-safari  margin-r-5"></i> 
	            		<b><?= __('WhereIsWhere Admin Site') ?></b>
	            	</strong> 
	            	<a class="pull-right"><?=$env?><strong><?= $company->sub_domain; ?></strong></a>
				</li>
	        	<li class="list-group-item">
	            	<strong>
	            		<i class="fa fa fa-file-pdf-o  margin-r-5"></i> 
	            		<b><?= __('Company Letter') ?></b></strong> 
	            		<a class="pull-right" target="_blank" href="<?=$filePath?>" ><?= $arrFile['name']; ?></a>
				</li>
				<li class="list-group-item">
					<?= $this->Html->link(__('Edit Other Details'), ['action' => 'editOtherDetails',$company->uuid],['class'=>'label label-warning', 'data-toggle'=>"modal", 'data-target'=>"#modalEditCompanyOtherDetails", 'id'=>"btnEditCompanyOtherDetails"] ) ?>
				</li>								
			</ul>		
		</div>	

	</div>
	<!-- /.box-body -->
</div>
<!-- /.box -->
<?= $this->element('modals',  ['id'=>'modalEditCompanyOtherDetails','modalTitle'=>'Update Other Details','size'=>'modal-lg'])?>
