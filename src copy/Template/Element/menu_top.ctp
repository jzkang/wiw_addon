<?php use Cake\Routing\Router; ?>

<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>WIW</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?=$defaultAppName?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <?php if ( 'sa' === $user['role']):?>
      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNewVendor" id="btnNewVendor" style="margin-top:7px">
      	<strong>New Vendor <font size=3>+</font></strong>
      </button>
      <?php endif;?>
      

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=$defaultImgProfile?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= $user['first_name'] . ' ' . $user['last_name']?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=$defaultImgProfile?>" class="img-circle" alt="User Image">

                <p>
                  <?= $user['first_name'] . ' ' . $user['last_name']?>
                  <small>Member since <?= date('M. Y', strtotime($user['created']))?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!--
              <li class="user-body">
               
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li>
              -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=Router::url(['controller'=>'companyUsers','action'=>'logout']) ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>

<?= $this->element('modals',  ['id'=>'modalNewVendor','modalTitle'=>'New Vendor'])?>  