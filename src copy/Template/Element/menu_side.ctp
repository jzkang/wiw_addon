<?php 
use Cake\Routing\Router; 
?>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><?=__('MAIN NAVIGATION')?></li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-reorder"></i> <span><?=__('Company Menus')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li class="<?php echo ('campaignNearbyPromotions' === strtolower($this->name) && 'index' === $this->request->action) ? 'active' : '' ?>">
              <a href="<?=Router::url(['controller'=>'campaignNearbyPromotions','action'=>'index', $companyUuid ]) ?>"><i class="fa  fa-cogs"></i><?=__('Nearby Promotions')?></a>
            </li>
            <li class="<?php echo ('campaignBusinessGroups' === strtolower($this->name) && 'index' === $this->request->action) ? 'active' : '' ?>">
              <a href="<?=Router::url(['controller'=>'companyBusinessGroups','action'=>'index', $companyUuid ]) ?>"><i class="fa  fa-cogs"></i><?=__('Business Groups')?></a>
            </li>

          	<li class="<?php echo ('companies' === strtolower($this->name) && 'view' === $this->request->action) ? 'active' : '' ?>">
          		<a href="<?=Router::url(['controller'=>'companies','action'=>'view', $companyUuid ]) ?>"><i class="fa  fa-briefcase"></i><?=__('Company Profile')?></a>
          	</li>
          </ul>
        </li>        
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-support"></i> <span><?=__('Support')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li class="">
          		<a href="mailto:<?=$emailSupport?>"><i class="fa fa-envelope"></i>Email</a>
          	</li>
            <li class="">
            	<a href="tel:+6597545389"><i class="fa fa-phone"></i>Contact</a>
            </li>
            <!-- 
            <li class="">
            	<a href="https://www.facebook.com/F-o-o-d-t-r-i-P-H-774500409375136/messages/" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
            </li>
             -->            
          </ul>
        </li>        
      </ul>