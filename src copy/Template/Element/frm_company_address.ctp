<?php 
// if (empty($company))
// 	$company = new App\Model\Entity\Company();
// if (empty($action))

$action = 'addAddress';

//debug($company);

?>
<div class="box box-primary" >
		<div class="modal-header <?=(@$missingAddress) ? 'bg-red' : ''?>">
			<h4 class="modal-title" id="myModalLabel"><?= __('Company Address')?></h4>
		</div>
		<div class="box-body">
			<?= $this->Form->create($company,['class'=>"form-horizontal",'url'=>['controller'=>'companies','action'=>$action,$company->uuid]]) ?>
		    <?php 
		    	//Hidden Fields
		    	echo $this->Form->hidden('id',['value'=>$company->id]);
		    ?>
					<div class="box-body">         		                		
						<div class="form-group">
							<div class="col-md-3">                	
								<label for="inputAddress1" class="control-label">Unit #</label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('address1', ['label'=>false,'type'=>'text','class'=>'form-control','placeholder'=>'e.g. 17-03 or leave blank if no unit number']);?>
	                    		<p class="help-block"><?=__('This not required')?></p>
	                  		</div>
                		</div>
                		
						<div class="form-group">   
							<div class="col-md-3">             	
								<label for="inputAddress2" class="control-label"><?=__('House # / Building Name')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('address2', ['label'=>false,'type'=>'text','class'=>'form-control', 'placeholder'=>'e.g. Laureano di Trevi Towers', 'required']);?>
	                  		</div>
                		</div>

						<div class="form-group">  
							<div class="col-md-3">              	
								<label for="inputStreet" class="control-label"><?=__('Street / Road / Avenue')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('street', ['label'=>false,'type'=>'text','class'=>'form-control', 'placeholder'=>'e.g. Chino Roces Ave, Pasong Tamo', 'required']);?>
	                  		</div>
                		</div>
                		
						<div class="form-group">
							<div class="col-md-3">                       	
								<label for="inputCity" class=" control-label"><?=__('City')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('city', ['label'=>false,'type'=>'text','class'=>'form-control', 'required']);?>
	                  		</div>
                		</div>                		
                		
						<div class="form-group">
							<div class="col-md-3">                       	
								<label for="inputCity" class=" control-label"><?=__('State')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('state', ['label'=>false,'type'=>'text','class'=>'form-control', ]);?>
	                  		</div>
                		</div>                		

						<div class="form-group">  
							<div class="col-md-3">                     	
								<label for="inputPostCode" class=" control-label"><?=__('Postal Code')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('postal_code', ['label'=>false, 'type'=>'text', 'class'=>'form-control', 'required']);?>
	                  		</div>
                		</div>                		
                		
						<div class="form-group">             
							<div class="col-md-3">          	
								<label for="inputCountry" class=" control-label"><?=__('Country')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->select('country_code',$Country,['default'=>$Country[$company->country_code], 'label'=>false, 'type'=>'text', 'class'=>'form-control', 'required']);?> 
	                  		</div>
                		</div>                		

						<div class="form-group">         
							<div class="col-md-3">              	
								<label for="inputCountry" class=" control-label"><?=__('Landmark')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('landmark', ['label'=>false, 'type'=>'text', 'class'=>'form-control','placeholder'=>'e.g. Offwhite building near Don Bosco']);?>
	                  		</div>
                		</div>                		
              		</div>
              		
	              <div class="box-footer">
	                <button type="reset" class="btn btn-default">Reset</button>
	                <?php if($this->request->isAjax):?>
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <?php endif;?>
	                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right']) ?>
	                <?= $this->Form->end() ?>
	              </div>
				<!-- /.box-footer -->
		</div>	
</div>