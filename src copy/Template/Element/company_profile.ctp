<?php
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Core\Configure;
 
$arrPhoto = json_decode($company->logo,true);
$imgPath = $defaultImg;
if ( !empty($arrPhoto) ) $imgPath = $this->request->webroot . $arrPhoto['path']. DS .$arrPhoto['name'];

//status display
$strStatusActive = '';
$strStatusInActive = 'active';
$strStatus = '<font class="text-red">(Closed)</font>';
if ($company->status) {
	$strStatusActive = 'active';
	$strStatusInActive = '';
	$strStatus = '<font class="text-green">(Open)</font>';
}

$strPublicUrl =  $this->request->webroot . "provider/".urlencode($company->name)."-".$company->uuid;
?>

<!-- Profile Image -->
<div class="box box-primary">
	<div class="box-header with-border">
		<i class="fa fa-info"></i>
		<h3 class="box-title">Company Details</h3>
    </div>
	<div class="box-body box-profile row">
		<div class=" col-md-4  "> 
			<a href="<?=Router::url(['controller'=>'companies','action'=>'ajxEditPhoto',$company->uuid])?>" data-toggle="modal" data-target="#modalEditVendorPhoto" class="company-image" id="btnEditVendorPhoto" uuid="<?=$company->uuid?>" >
				<img alt="Vendor Image" src="<?=$imgPath?>" class='img-responsive img-center'>
			</a>
			<small class="help-block text-center">( click the image to change )</small>
			<h3 class="profile-username text-center"><?= h($company->name); ?> <span class="company-status" ><?= $strStatus?></span></h3>
			<ul class="list-group list-group-unbordered">
	        	<li class="list-group-item ">
	            	<strong><i class="fa fa fa-power-off margin-r-5"></i> <b>Status</b></strong> 
					<div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
						
	                	<div class="btn-group" data-toggle="btn-toggle">
	                  		<button type="button" status="1" class=" btnVendorStatus btn btn-default btn-sm <?= $strStatusActive?>"><i class="fa fa-square text-green"> </i>
	                  		</button>
	                  		<button type="button" status="0" class=" btnVendorStatus btn btn-default btn-sm <?= $strStatusInActive?>"><i class="fa fa-square text-red"> </i></button>
	                	</div>
	                	<script>
							$(".btnVendorStatus").click(function(e){
								e.preventDefault();
							    $.post("<?=Router::url(['controller'=>'companies','action'=>'status',$company->uuid]) ?>",
							    	    {status: $(this).attr('status')},
							    	    function(data, status){
							    	        //console.log("Data: " + data + "\nStatus: " + status);
							    	        if (data == '1') $(".company-status").html('<font class="text-green">(Open)</font>');
							    	        else $(".companies-status").html('<font class="text-red">(Closed)</font>');
							    	    });
	
	
							});
	                	</script>

	              	</div>            	
				</li>	
			</ul>					
		</div>
		<div class="col-md-8"> 
			<ul class="list-group list-group-unbordered">
	        	<li class="list-group-item">
	            	<strong><i class="fa fa fa-suitcase  margin-r-5"></i> <b><?= __('Business Type') ?></b></strong> <a class="pull-right"><?= Configure::read('BusinessType')[$company->type] ; ?></a>
				</li>			
	        	<li class="list-group-item">
	            	<strong><i class="fa fa fa-calendar  margin-r-5"></i> <b><?= __('Date Joined') ?></b></strong> <a class="pull-right"><?= date('d-M-Y',strtotime($company->created)); ?></a>
				</li>
				<li class="list-group-item">
					<strong><i class="fa fa fa-envelope margin-r-5"></i> <b><?=__('Company Email')?></b></strong> <a class="pull-right " href="mailto:<?=$company->contact_email;?>" ><?= h($company->contact_email) ?></a>
				</li>
				<li class="list-group-item">
					<strong><i class="fa fa fa-phone margin-r-5"></i> <b><?=__('Contact Number')?></b></strong> <a class="pull-right"><?= h($company->contact_num) ?></a>
				</li>
	
				<li class="list-group-item">
					<strong><i class="fa fa fa-file-text margin-r-5"></i> <b><?=__('Company Short Write-up')?></b></strong> <p><?= h($company->description_long) ?></p>
				</li>
				<li class="list-group-item">
					<?= $this->Html->link(__('Edit Profile'), ['action' => 'edit',$company->uuid],['class'=>'label label-warning', 'data-toggle'=>"modal", 'data-target'=>"#modalEditCompanyProfile", 'id'=>"btnEditVendor"] ) ?>
					<!-- 
					<?= $this->Html->link(__('Add Menu'), ['controller'=>'menus', 'action' => 'index',$company->uuid],['class'=>'label label-success', 'id'=>"btnAddMenu"] ) ?>
					<?= $this->Html->link(__('Add Menu Add-ons'), ['controller'=>'menu-add-ons', 'action' => 'index',$company->uuid],['class'=>'label label-success', 'id'=>"btnAddMenu"] ) ?>
					
					<?= $this->Html->link(__('Add User'), ['controller'=>'menus', 'action' => 'add',$company->id],['class'=>'label label-success', 'data-toggle'=>"modal", 'data-target'=>"#modalAddMenu", 'id'=>"btnAddMenu"] ) ?>
					 -->				
				</li>
				<!-- 
				<li class="list-group-item">
					<div class="fb-share-button" data-href="<?=$strPublicUrl?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=$strPublicUrl?>&amp;src=sdkpreparse">Share</a></div>
				</li>
				 -->
				
				<li class="list-group-item">
					<a href="<?= empty($company->url) ? '':h($company->url) ?>" <?= empty($company->url) ? 'disabled':''?> target="_blank" class="btn btn-primary btn-block">
						<i class="fa fa fa-safari margin-r-5"></i>
						<b><?=__('Company Website')?></b>
					</a>				
				</li>
			</ul>		
		</div>	
		
		<div class="col-md-8"> 
			<?php if( empty($company->company_letter) || empty($company->sub_domain) || empty($company->invite_code)):?>
				<?= $this->element('frm_company_other_details');?>
			<?php else:?>			
				<?= $this->element('company_other_details');?>
			<?php endif;?>
		</div>

		
		


	</div>
	<!-- /.box-body -->
</div>
<!-- /.box -->
<?= $this->element('modals',  ['id'=>'modalEditCompanyProfile','modalTitle'=>__('Update Company Profile')])?>
<?= $this->element('modals',  ['id'=>'modalEditVendorPhoto','modalTitle'=>'Update Photo'])?>

<div id="fb-root"></div>
<script>
/*
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1185406864918759";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
*/
</script>
