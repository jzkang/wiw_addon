    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
<?php if ( !empty($vendorAddress) ): ?>
<div class="box box-primary">
	<div class="box-body box-profile">

		<!-- <h3 class="profile-username text-center"><?= $vendorAddress->address_name;?><br><small>( <?=$vendorAddress->uuid?> )</small></h3>  -->
		<h3 class="profile-username">Store Address</small></h3>
        <div class="row">
			<div class="col-md-4">
		    	<strong>
					<i class="fa fa fa-map-marker  margin-r-5"></i>
					<a target="_blank" href="http://maps.google.com/?daddr=<?=$vendorAddress->latitude.','.$vendorAddress->longitude?>"> Map</a>
				</strong>
            	<span>
	       			<p class="text-muted">
	       				<?= empty($vendorAddress->address1) ? "" : h($vendorAddress->address1) . ", "?>
	       				<?= empty($vendorAddress->address2) ? "" : h($vendorAddress->address2) ?>
	       				<?= empty($vendorAddress->street) ? "" : " ".h($vendorAddress->street) ?>
	       				<?= empty($vendorAddress->city) ? "" : "<br>".h($vendorAddress->city) ?>
	       				<?= empty($vendorAddress->state) ? "" : "<br>".h($vendorAddress->state) ?>
	       				<?= empty($vendorAddress->country) ? "" : "<br>".h($Country[$vendorAddress->country]) ?>
	       				<?= empty($vendorAddress->post_code) ? "" : "<br>".h($vendorAddress->post_code) ?>
		                <?= empty($vendorAddress->landmarks) ? "" : "<br><u>Landmark</u>: ".h($vendorAddress->landmarks) ?>
	       			</p>
            	</span>					
			</div>
			<div class="col-md-8">
		    	<div id="map"></div>
			    <script>
			      function initMap() {
			        var uluru = {lat: <?=$vendorAddress->latitude?>, lng: <?=$vendorAddress->longitude?>};
			        var map = new google.maps.Map(document.getElementById('map'), {
			          zoom:15,
			          center: uluru
			        });
			        var marker = new google.maps.Marker({
			          position: uluru,
			          map: map
			        });
			      }
			    </script>
		    	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9dQz2vPKrVevVsQPy4QP-wCJXO_5cCtY&callback=initMap"></script>
			</div>				            	
		</div>
	</div>
</div>
<?php endif;?>