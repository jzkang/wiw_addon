<?php 
// if (empty($company))
// 	$company = new App\Model\Entity\Company();
// if (empty($action))

$action = 'addOtherDetails';

//debug($company);

?>
<div class="box box-primary" >
		<div class="modal-header">
			<h4 class="modal-title" id="myModalLabel"><?= __('Other Details')?></h4>
		</div>
		<div class="box-body">
			<?= $this->Form->create($company,['enctype' => 'multipart/form-data', 'class'=>"form-horizontal",'url'=>['controller'=>'companies','action'=>$action,$company->uuid]]) ?>
		    <?php 
		    	//Hidden Fields
		    	echo $this->Form->hidden('id',['value'=>$company->id]);
		    ?>
					<div class="box-body">         		                		
						<div class="form-group">
							<div class="col-md-3">                	
								<label for="inputInviteCode" class="control-label"><?=__('Invitation Code')?></label>
							</div>					
							<div class="col-md-9">
	                    		<?= $this->Form->input('invite_code', ['label'=>false,'type'=>'text','class'=>'form-control','placeholder'=>'']);?>
	                  		</div>
                		</div>
                		
						<div class="form-group">   
							<div class="col-md-3">             	
								<label for="inputUrl" class="control-label"><?= $defaultAppName . ' ' . __('Website')?></label>
							</div>
							<div class="col-md-4" style="padding-top: 5px;">
								<span><?=$env?></span>
							</div>					
							<div class="col-md-5">
	                    		<?= $this->Form->input('sub_domain', ['label'=>false,'type'=>'text','class'=>'form-control', 'placeholder'=>'', 'required']);?>
	                  		</div>
                		</div>
                		
						<div class="form-group">
							<div class="col-md-3">                	
								<label for="inputInviteCode" class="control-label"><?=__('Company Letter')?></label>
							</div>					
							<div class="col-md-9" style="padding-top: 5px;">
	                    		<?= $this->Form->input('company_letter', ['label'=>false,'type'=>'file','class'=>'','placeholder'=>'']);?>
	                    		<small class="help-block text-left"><?=__('PDF File only')?></small>
	                  		</div>
                		</div>                		
                		
              		</div>
              		
	              <div class="box-footer">
	                <button type="reset" class="btn btn-default">Reset</button>
	                <?php if($this->request->isAjax):?>
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <?php endif;?>
	                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right']) ?>
	                <?= $this->Form->end() ?>
	              </div>
				<!-- /.box-footer -->
		</div>	
</div>