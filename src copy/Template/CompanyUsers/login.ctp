<?php use Cake\Routing\Router;?>
<?php $this->assign('title', __('Sign In'))?>
<div class="login-box-body">
    <p class="login-box-msg"><?=__('Sign in to manage your merchants')?></p>
    <?= $this->Form->create($companyUser,array('class'=>"")) ?>
    <form action="merchant-users/login" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">        
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <?php if($blnCaptcha):?>
      <input type="hidden" name="reCaptcha" value=1>
      <div class="g-recaptcha" data-sitekey="6Ld9RRgUAAAAAEjl99aPJUewJs0LC6SB9lpNBgdC"></div>
      <?php endif;?>
            <br>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox">&nbsp;&nbsp;<?=__('Remember Me')?>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?= $this->Form->button(__('Sign In'),['type'=>'submit', 'class'=>'btn btn-primary btn-block btn-flat pull-right']) ?>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="<?=Router::url(['controller'=>'company-users','action'=>'forgotPassword']);?>">I forgot my password</a><br>    
    <span>Are you not a seller yet? </span>
    <a href="<?=Router::url(['controller'=>'accounts','action'=>'create']);?>" class="" data-toggle="modal" data-target="#modalNewMerchant" id="btnNewMerchant" >Sign up now!</a><br>
    <?= $this->element('modals',  ['id'=>'modalNewMerchant','modalTitle'=>'Create Merchant Account','size'=>'modal-md'])?>
    <?= $this->Form->end() ?>
</div>