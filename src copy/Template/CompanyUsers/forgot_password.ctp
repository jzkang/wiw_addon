<?php use Cake\Routing\Router;?>
<?php $this->assign('title', 'Forgot Password')?>
<div class="login-box-body">
    <p class="login-box-msg">Key-in your email and we will send your temporary password!</p>

    <?= $this->Form->create($companyUser,array('class'=>"")) ?>
    
    <form action="vendor-users/login" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        
        <br>
        <!-- reCaptcha -->
        <div class="g-recaptcha" data-sitekey="6Ld9RRgUAAAAAEjl99aPJUewJs0LC6SB9lpNBgdC"></div>
      </div>

      <div class="row">
        <div class="col-xs-8">
			<span>Click <a href="<?=Router::url(['controller'=>'company-users','action'=>'login']);?>">here to login</a> to your account.</span>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?= $this->Form->button(__('Reset'),['type'=>'submit', 'class'=>'btn btn-primary btn-block btn-flat pull-right']) ?>
        </div>
        <!-- /.col -->
        
     
        
      </div>
      
    </form>
    <?= $this->Form->end() ?>
</div>