<div class="login-box-body">
    <p class="login-box-msg">Change your password</p>

    <?= $this->Form->create($companyUser,array('class'=>"")) ?>
    
    <form action="vendor-users/login" method="post">

      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" id="password">        
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <label><input id="methods" type="checkbox"/> &nbsp;Show password</label>  
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
&nbsp;
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?= $this->Form->button(__('Update'),['type'=>'submit', 'class'=>'btn btn-primary btn-block btn-flat pull-right']) ?>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
    <?= $this->Form->end() ?>
</div>
<script>
$("#methods").change(function(){
    if (this.checked) $("#password").attr('type','text');
    else $("#password").attr('type','password');
});
</script>