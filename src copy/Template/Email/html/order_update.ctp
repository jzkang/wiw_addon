<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    <title>Foundation for Sites</title>-->
        <link rel="stylesheet" href="../css/foundation.min.css">
        <link rel="stylesheet" href="../css/app.css">    
    </head>
    <body>


      <!-- title row -->
        <h2>
        Order Update  as of <small><?=date('d/M/Y h:ia');?></small>
        </h2>

      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <u>Seller:</u>
          <address>
            <b><?=$strVendorName?></b><br>
            Email: <a href="mailto:<?=$strVendorEmail?>"><?=$strVendorEmail?></a><br>
            Contact #: <a href="tel:<?=$strVendorContactNum?>"><?=$strVendorContactNum?></a><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <br>
          <u>Buyer:</u>
          <address>
            <b><?=$strFirstNameUser?></b><br>
            Email: <a href="mailto:<?=$strUserEmail?>"><?=$strUserEmail?></a><br>
            Contact #: <a href="tel:<?=$strUserContactNum?>"><?=$strUserContactNum?></a><br>          
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        	<br>
        	<span><u>Ordert Details</u></span>
          <br>
          <b>Order ID:&nbsp; </b> <span><?=$strRefId?></span><br>
          <b>Ordered On:&nbsp;  </b> <span><?=$strTransacDate?></span> <br>
          <b>Payment: &nbsp; </b> <span><?=$strPaymentMethod?></span><br>
          <b>Meetup: &nbsp;</b> <span><?=$strMeetup?></span><br>
          <b>Note to Seller: &nbsp;</b> <span><?=$strRemarks?></span><br>
          <b>Status:&nbsp;</b> <?=$strOrderStatus?> <br>
        	<?php if (!empty($strReason)):?>
            Reason: <font color="red"><strong><?=$strReason?></strong></font> <br>
            <?php endif;?>          
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
 
        <br>
        <?=$this->element('email_item_list')?>  
           
        <br><br>
        <p>
        <b>foodtri</b>PH Team - home cooked food at your fingertips
        </p>
    </body>
</html>     