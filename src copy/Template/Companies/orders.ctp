<?php 
$this->assign('title', h('Orders'));
$arrOrdersNew = !empty($arrTransactions[1]) ? $arrTransactions[1] : null;
$arrOrdersAccepted = !empty($arrTransactions[2]) ? $arrTransactions[2] : null;
$arrOrdersPaid = !empty($arrTransactions[3]) ? $arrTransactions[3] : null;
$arrOrdersPreparing = !empty($arrTransactions[4]) ? $arrTransactions[4] : null;
$arrOrdersCompleted = !empty($arrTransactions[5]) ? $arrTransactions[5] : null;
$arrOrdersCancelled = !empty($arrTransactions[0]) ? $arrTransactions[0] : null;

use Cake\Routing\Router;
extract(Router::parseNamedParams($this->request)->params['named']);
if (empty($refTab)) $refTab = 'NewOrders';
?>
<div class="row">
	<section class="content">
		<div class="row">
			<div class="col-md-12">

					<div class="row">
					
						<div class="col-md-12">
							<div class="nav-tabs-custom">
            					<ul class="nav nav-tabs">
              						<li class="<?=($refTab === 'NewOrders') ? 'active' : '';?>"><a href="#NewOrders" data-toggle="tab" aria-expanded="false">New Orders</a></li>
              						<li class="<?=($refTab === 'PendingOrders') ? 'active' : '';?>"><a href="#PendingOrders" data-toggle="tab" aria-expanded="false">Accepted Orders</a></li>
              						<li class="<?=($refTab === 'CompletedOrders') ? 'active' : '';?>"><a href="#CompletedOrders" data-toggle="tab" aria-expanded="false">Completed Orders</a></li>
              						<li class="<?=($refTab === 'CancelledOrders') ? 'active' : '';?>"><a href="#CancelledOrders" data-toggle="tab" aria-expanded="false">Cancelled Orders</a></li>
              						
            					</ul>
            					<div class="tab-content">
              						<div class="tab-pane <?=($refTab === 'NewOrders') ? 'active' : '';?>" id="NewOrders">              							
										<?= $this->element('orders_list', ['arrOrders'=>$arrOrdersNew, 'title'=>'New Orders', 'tableId'=>'tblOrdersNew' ]) ?>
                					</div>
              
              						<div class="tab-pane <?=($refTab === 'PendingOrders') ? 'active' : '';?>" id="PendingOrders">              							
										<?= $this->element('orders_list', ['arrOrders' => $arrOrdersAccepted, 'title'=>'Accepted Orders', 'tableId'=>'tblOrdersPending']) ?>
									</div>
									
              						<div class="tab-pane <?=($refTab === 'CompletedOrders') ? 'active' : '';?>" id="CompletedOrders">
										<?= $this->element('orders_list', ['arrOrders'=>$arrOrdersCompleted, 'title'=>'Completed Orders', 'tableId'=>'tblOrdersCompleted']) ?>
									</div>
									
              						<div class="tab-pane <?=($refTab === 'CancelledOrders') ? 'active' : '';?>" id="CancelledOrders">
										<?= $this->element('orders_list', ['arrOrders'=>$arrOrdersCancelled, 'title'=>'Cancelled Orders', 'tableId'=>'tblOrdersCancelled']) ?>
									</div>									
				
            					</div>
          					</div>						
						
						</div>
					</div>
				       
	        	</div>	  
			
		</div>
	</section>
</div>