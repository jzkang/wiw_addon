<?php 
$this->assign('title', 'Partner Seller');
?>
<div class="row">
	
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
	<section class="content">

		<div class="row">
        	<div class="col-md-12">
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Seller Address -->
		        		<?php if (!$vendorAddress->latitude && !$vendorAddress->longitude):?>
		        		<?= $this->element('frm_vendor_address', ['vendorAddress' => $vendorAddress, 'action' => 'add']);?>
		        		<?//php else:?>
						<?//= $this->element('vendor_address_profile', compact('vendorAddress') );?>
						<?php endif;?>        			
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Meetup Timing and Location -->
		        		<?php if (empty($vendor->menus)):?>
		        		<?= $this->element('frm_menu');?>
						<?php endif;?>        			
        			</div>
        		</div>        		
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Meetup Timing and Location -->
		        		<?php if (empty($vendorAddress->operating_hours)):?>
		        		<?= $this->element('frm_vendor_operation');?>
		        		<?//php else: ?>
						<?//= $this->element('vendor_operation', compact('vendorAddress') );?>
						<?php endif;?>        			
        			</div>
        		</div>        		        		
	        </div>	        
		</div>
	</section>
</div>
	