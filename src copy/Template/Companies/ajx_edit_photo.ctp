<?php
$arrPhoto = json_decode($company->logo,true);
$imgPath = $defaultImg;
if ( !empty($arrPhoto) ) $imgPath = $this->request->webroot . $arrPhoto['path']. DS .$arrPhoto['name'];
?>
<?= $this->Form->create($company,['id'=>'frmEditPhoto', 'class'=>"form-horizontal",'type' => 'file']) ?>
<div class="box box-primary">
	<div class="box-body box-profile">
		<?=$this->element('alert_info',['message'=> __('This will be the primary photo of the company and will be displayed in '.$defaultAppName.' App')]); ?>				              
		<div class="row">
			<div class="col-md-4">
				<img class="img-responsive" alt="Vendor Image" src="<?=$imgPath?>">								
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label for="inputPhoto1" class="col-sm-3 control-label"><?=__('Company Logo')?></label>
					<div class="col-sm-9">
						<?= $this->Form->file('logo',['id'=>'inputPhoto1', 'required'=>true])?>
						<p class="help-block">Image size within 600x400 and .JPG or .PNG only</p>
					</div>
				</div>

			</div>	
		</div>
		
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="submit" id="btnUpdatePhoto" class="btn btn-primary">Update</button>
</div>
<?= $this->Form->end() ?>