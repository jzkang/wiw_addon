<?php 
$this->assign('title', $vendor->name);
$this->assign('description', $vendor->description);
?>
<div class="row">
	
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
	<section class="content">

		<div class="row">
        	<div class="col-md-4">
				<?= $this->element('public_vendor_profile');?>
	        </div>
        	<div class="col-md-8">
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Seller Address -->
						<?= $this->element('public_vendor_address_profile', compact('vendorAddress') );?>		
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Meetup Timing and Location -->
						<?= $this->element('public_vendor_operation', compact('vendorAddress') );?>
        			</div>
        		</div>        		
        		<div class="row">
        			<div class="col-md-12">
						<div class=" ">
							<div class="box-header with-border">
				            	<h3 class="box-title pull-left">Menus</h3>
				            </div>
						</div>
        				<!-- Menus -->
						<?= $this->element('public_vendor_menus', compact('menus') );?>
        			</div>
        		</div>        		
	        </div>	        
		</div>
	</section>
</div>
	