<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Company Business Locations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="companyBusinessLocations form large-9 medium-8 columns content">
    <?= $this->Form->create($companyBusinessLocation) ?>
    <fieldset>
        <legend><?= __('Add Company Business Location') ?></legend>
        <?php
            echo $this->Form->input('uuid');
            echo $this->Form->input('company_id', ['options' => $companies, 'empty' => true]);
            echo $this->Form->input('company_uuid');
            echo $this->Form->input('business_name');
            echo $this->Form->input('company_business_category_id');
            echo $this->Form->input('company_business_sub_categories');
            echo $this->Form->input('mall_id');
            echo $this->Form->input('ccc_id');
            echo $this->Form->input('logo');
            echo $this->Form->input('is_company_logo');
            echo $this->Form->input('latitude');
            echo $this->Form->input('longitude');
            echo $this->Form->input('pin_latitude');
            echo $this->Form->input('pin_longitude');
            echo $this->Form->input('place_id');
            echo $this->Form->input('address1');
            echo $this->Form->input('address2');
            echo $this->Form->input('street');
            echo $this->Form->input('city');
            echo $this->Form->input('state');
            echo $this->Form->input('country');
            echo $this->Form->input('country_code');
            echo $this->Form->input('postal_code');
            echo $this->Form->input('unit_num');
            echo $this->Form->input('landmarks');
            echo $this->Form->input('is_company_contact');
            echo $this->Form->input('contact_person');
            echo $this->Form->input('contact_email');
            echo $this->Form->input('contact_num');
            echo $this->Form->input('operating_hours');
            echo $this->Form->input('currency');
            echo $this->Form->input('status');
            echo $this->Form->input('deleted');
            echo $this->Form->input('created_by');
            echo $this->Form->input('modified_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
