<?php use Cake\Routing\Router; ?>

<div class="row">	
	<div class="col-md-12">
		<a href="<?=Router::url(['controller'=>'menus','action'=>'add',$vendorUuid]) ?>" class="btn bg-olive btn-flat col-md-2 col-xs-12" data-toggle="modal" data-target="#modalNewMenu" id="btnNewMenu" vendor-uuid="<?=$vendorUuid?>">
			Add Menu
		</a>
	</div>
</div>
<div class="row"><div class="col-md-12">&nbsp;</div></div>

<?= $this->element('modals',  ['id'=>'modalNewMenu','modalTitle'=>'Add New Menu','size'=>'modal-lg'])?>
<?= $this->element('vendor_menus', ['arrVendorMenus' => $menus]) ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1185406864918759";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>