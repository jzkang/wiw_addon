<?php $this->assign('title', 'Add Flash Nearby Promotion')?>
<div class="row">
    <div class="col-md-12 form-group">
        <section class="panel">
            <div class="panel-body">
                <?= $this->Form->create($campaignNearbyPromotion,['class'=>"form-horizontal",'url'=>['controller'=>'campaignNearbyPromotions','action'=>$action ,$companyUuid]]) ?>
                     <?php 
                        //Hidden Fields
                        echo $this->Form->hidden('companyUuid',['value'=>$companyUuid]);
                    ?>    

                    <div class="form-group">
                        <label class="col-sm-2">Launch It Now?</label>
                        <div class="col-sm-10">
                        <?= 
                            $this->Form->checkbox('launched', ['value'=>'1', 'hiddenField' => false, 'required'=>false, 'class'=>'ctime']);
                        ?> Yes. <i>Promotions during merchant operating hours.</i>
                        </div>
                    </div>

                    <div id="show_time">
                        <!-- time Picker -->
                        
                        <div class="form-group">
                            <label class="col-md-2">Start Time</label>
                            <div class="col-md-3 col-sm-4">
                                <div class="bootstrap-timepicker">
                                    <div class="input-group">
                                        <?= $this->Form->input("promo_time[]", ['label'=>false, 'type'=>'text', 'value'=>'10:00 AM', 'class'=>'form-control timepicker','placeholder'=>""]);?>

                                      <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        
                        <div class="form-group">
                            <label class="col-md-2">End Time</label>
                            <div class="col-md-3 col-sm-4">
                                <div class="bootstrap-timepicker">
                                    <div class="input-group">
                                      <?= $this->Form->input("promo_time[]", ['label'=>false, 'type'=>'text', 'value'=>'10:00 PM', 'class'=>'form-control timepicker','placeholder'=>""]);?>
                  
                                      <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>

                    <div class="form-group">
                    <label class="col-sm-2">Promotion Name</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input("promotion_name", ['label'=>false, 'type'=>'text', 'value'=>'', 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"100", 'required'=>"required"]);?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="col-sm-2">Description</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input("description", ['label'=>false, 'type'=>'text', 'value'=>'', 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"100", 'required'=>"required"]);?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2">Launch By</label>
                        <div class="col-sm-10">
                        <?= 
                            $this->Form->radio(
                                'r1',
                                [
                                    ['value' => '0', 'text' => 'Business Group','id'=>'r1','class'=>'r1', 'checked'=>true, 'style'=>'margin: 0px 10px;'],
                                    ['value' => '1', 'text' => 'Business Location','id'=>'r2','class'=>'r1','style'=>'margin: 0px 10px;']
                                ]
                            );

                        ?>
                        </div>
                    </div>

                    <div class="form-group" id="show_bu_group" style="display: block;">
                        <label class="col-md-2">
                            Choose Business Group to launch
                        </label>
                        <div class="col-md-10">
                            
                            <table>
                                <tr style="border-bottom: solid 1px #dbdbdb;">
                                    <td width="100%"><b>Group Name</b></td>
                                </tr>
                                <?php foreach ($companyBusinessGroups as $group): ?>
                                    <tr style="border-bottom: solid 1px #dbdbdb;">
                                        <td style="padding: 5px 0px;">
                                            <?=
                                                $this->Form->checkbox('bu_group[]', ['value'=>$group->id, 'hiddenField' => false]);
                                            ?> <?= $group->group_name ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                
                            </table>
                            
                        </div>
                    </div>
                    
                    <div class="form-group" id="show_bu_location" style="display: none;">
                        <label class="col-md-2">
                            Choose location to launch
                        </label>
                        <div class="col-md-10">
                            <table>
                                <tr style="border-bottom: solid 1px #dbdbdb;">
                                    <td width="60%"><b>Business Name and Location</b></td>
                                    <td width="20%" style="text-align: center;"><i class="fa fa-fw fa-heartbeat" style="color: red;"></i><b>Heartbeat</b> <a href="#" data-toggle="tooltip" data-placement="top" title="Total Follower Nearby this business location"><i class="fa fa-fw fa-question-circle"></i></a></td>
                                    <td width="20%" style="text-align: center;"><b>Installed Users</b> <a href="#" data-toggle="tooltip" data-placement="top" title="Total Installed Consumers Nearby this business location"><i class="fa fa-fw fa-question-circle"></i></a></td>
                                </tr>
                                <?php foreach ($b_loc as $idx=>$loc): ?>
                                    <tr style="border-bottom: solid 1px #dbdbdb;">
                                        <td style="padding: 5px 0px;">
                                            <?=
                                                $this->Form->checkbox('bu_loc[]', ['value'=>$idx, 'hiddenField' => false]);
                                            ?> <?= $loc ?>

                                        </td>
                                        <td style="text-align: center;"><?= rand(100,3000); ?></td>
                                        <td style="text-align: center;"><?= rand(5000,10000); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                
                            </table>
                            
                        </div>
                    </div>

                    <br style="clear: both;">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <?= $this->Form->button(__('Save'), ['class'=>"btn btn-info"]) ?>
                            <?= $this->Html->link(__('Back'), ['action' => 'index',$companyUuid], ['class'=>"btn btn-info"]) ?>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
    </div>
</div>

<script>
    $(function(){
        
        $('[data-toggle="tooltip"]').tooltip();
        
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
        
        var ini_val = function(){
            $("#r1").prop("checked",true);
        }

        var reset = function(){
            $("#r1").prop("checked",false);
            $("#r2").prop("checked",false);
        }

        var onoff = function(){
            //ini_val();
            
            var rv=$('[name="r1"]:checked').val();
            if (rv=="0"){
                $("#show_bu_group").css("display","block");
                $("#show_bu_location").css("display","none");
               
            }else if(rv=="1"){
                $("#show_bu_group").css("display","none");
                $("#show_bu_location").css("display","block");
            }
        }
        
        
        
        $(".r1").on('change', function(e) {
            
            onoff();
        });
        $(".ctime").on('change', function(e) {
            
            if ($('input.ctime').is(':checked'))
            {
                $("#show_time").css("display","none");
            }else{
                $("#show_time").css("display","block");
            }
            
        });
    });

</script>