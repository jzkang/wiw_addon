<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $campaignNearbyPromotion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $campaignNearbyPromotion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Campaign Nearby Promotions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="campaignNearbyPromotions form large-9 medium-8 columns content">
    <?= $this->Form->create($campaignNearbyPromotion) ?>
    <fieldset>
        <legend><?= __('Edit Campaign Nearby Promotion') ?></legend>
        <?php
            echo $this->Form->input('uuid');
            echo $this->Form->input('company_id', ['options' => $companies, 'empty' => true]);
            echo $this->Form->input('company_uuid');
            echo $this->Form->input('promotion_name');
            echo $this->Form->input('description');
            echo $this->Form->input('images');
            echo $this->Form->input('business_locations');
            echo $this->Form->input('type');
            echo $this->Form->input('remarks');
            echo $this->Form->input('launched');
            echo $this->Form->input('active');
            echo $this->Form->input('deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
