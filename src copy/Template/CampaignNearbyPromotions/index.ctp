<?php $this->assign('title', 'Flash Nearby Promotions')?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">&nbsp;</h3>
        <?= $this->Html->link(__('Add Flash Nearby Promotion'), ['action' => 'add', $companyUuid], ['class'=>"btn btn-info"]) ?>
    </div>
    <!-- /.box-header -->

    <div class="box-body">

        <table id="dynamic-table-dept" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Promotion Name</th>
                    <th>Location Launch</th>
                    <th>Launched?</th>
                    <th>Time Period</th>
                    
                    <th class="center" width="50px" class="no-sort">Action</th>
                    
                </tr>
                </thead>
            
            <tbody>
            
            <?php foreach ($campaignNearbyPromotions as $campaignNearbyPromotion): ?>
            <tr>
                <td><?= $campaignNearbyPromotion->created ?></td>
                <td><?= $campaignNearbyPromotion->promotion_name ?></td>
                <td><?= $list['loc'] ?></td>
                <td><?= $campaignNearbyPromotion->launched ?></td>
                <td><?= $campaignNearbyPromotion->promo_time ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit-business-group', $campaignNearbyPromotion->id, $companyUuid], ['class'=>"btn btn-block btn-xs btn-info"]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $campaignNearbyPromotion->id, $companyUuid], ['confirm' => __('Are you sure you want to delete {0}?', $campaignNearbyPromotion->promotion_name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
