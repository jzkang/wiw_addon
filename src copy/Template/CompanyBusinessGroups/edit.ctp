<div class="row">
    <div class="col-md-12 form-group">
        <section class="panel">
            <div class="panel-body">
                <?= $this->Form->create($companyBusinessGroup,['class'=>"form-horizontal",'url'=>['controller'=>'companyBusinessGroups','action'=>$action ,$companyBusinessGroup->id]]) ?>
                     <?php 
                        //Hidden Fields
                        echo $this->Form->hidden('id',['value'=>$companyBusinessGroup->id]);
                    ?>    
                    <div class="form-group">
                    <label class="col-sm-2">Group Name</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input("group_name", ['label'=>false, 'type'=>'text', 'value'=>@$companyBusinessGroup->group_name, 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"45", 'required'=>"required"]);?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="col-sm-2">Description</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input("description", ['label'=>false, 'type'=>'text', 'value'=>@$companyBusinessGroup->description, 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"45", 'required'=>"required"]);?>
                    </div>
                    </div>
            
                    <div class="form-group">
                    <label class="col-sm-2">Select Business Location</label>
                    <div class="col-sm-10">
                    <!--
                        <table>
                        {foreach $this->bu_location as $b}
                            <tr style="border-bottom: solid 1px #dbdbdb;">
                            <td style="padding: 5px 0px;"><input type="checkbox" name="bu[]" value="{$b.uuid}" {if $this->rec.0.business_location}{if in_array($b.uuid,$this->rec.0.business_location)}checked{/if}{/if}> {$b.business_name}: {join(", ",$b.full_address)}</td>
                            <td></td>
                            <td></td>
                            </tr>
                        {/foreach}
                        </table>
                        -->
                    </div>
                    </div>

                    <br style="clear: both;">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-info">Save</button>
                            <a class="btn btn-info" href="adbugroup">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
    </div>
</div>

<script>
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });
});
</script>
<!--
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $companyBusinessGroup->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $companyBusinessGroup->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Company Business Groups'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="companyBusinessGroups form large-9 medium-8 columns content">
    <?= $this->Form->create($companyBusinessGroup) ?>
    <fieldset>
        <legend><?= __('Edit Company Business Group') ?></legend>
        <?php
            echo $this->Form->input('company_id', ['options' => $companies, 'empty' => true]);
            echo $this->Form->input('company_uuid');
            echo $this->Form->input('group_name');
            echo $this->Form->input('description');
            echo $this->Form->input('business_location');
            echo $this->Form->input('active');
            echo $this->Form->input('deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
-->