<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CampaignNearbyPromotionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CampaignNearbyPromotionsController Test Case
 */
class CampaignNearbyPromotionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.campaign_nearby_promotions',
        'app.companies',
        'app.company_business_categories',
        'app.company_business_locations',
        'app.malls',
        'app.cccs',
        'app.places',
        'app.company_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
