<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanyBusinessGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanyBusinessGroupsTable Test Case
 */
class CompanyBusinessGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanyBusinessGroupsTable
     */
    public $CompanyBusinessGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.company_business_groups',
        'app.companies',
        'app.company_business_categories',
        'app.company_business_locations',
        'app.company_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyBusinessGroups') ? [] : ['className' => 'App\Model\Table\CompanyBusinessGroupsTable'];
        $this->CompanyBusinessGroups = TableRegistry::get('CompanyBusinessGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyBusinessGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
