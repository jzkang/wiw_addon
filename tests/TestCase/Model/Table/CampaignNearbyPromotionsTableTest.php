<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CampaignNearbyPromotionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CampaignNearbyPromotionsTable Test Case
 */
class CampaignNearbyPromotionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CampaignNearbyPromotionsTable
     */
    public $CampaignNearbyPromotions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.campaign_nearby_promotions',
        'app.companies',
        'app.company_business_categories',
        'app.company_business_locations',
        'app.malls',
        'app.cccs',
        'app.places',
        'app.company_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CampaignNearbyPromotions') ? [] : ['className' => 'App\Model\Table\CampaignNearbyPromotionsTable'];
        $this->CampaignNearbyPromotions = TableRegistry::get('CampaignNearbyPromotions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CampaignNearbyPromotions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
