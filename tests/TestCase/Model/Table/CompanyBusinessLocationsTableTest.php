<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanyBusinessLocationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanyBusinessLocationsTable Test Case
 */
class CompanyBusinessLocationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanyBusinessLocationsTable
     */
    public $CompanyBusinessLocations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.company_business_locations',
        'app.companies',
        'app.company_business_categories',
        'app.company_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyBusinessLocations') ? [] : ['className' => 'App\Model\Table\CompanyBusinessLocationsTable'];
        $this->CompanyBusinessLocations = TableRegistry::get('CompanyBusinessLocations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyBusinessLocations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
