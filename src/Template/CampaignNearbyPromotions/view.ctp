<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Campaign Nearby Promotion'), ['action' => 'edit', $campaignNearbyPromotion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Campaign Nearby Promotion'), ['action' => 'delete', $campaignNearbyPromotion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $campaignNearbyPromotion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Campaign Nearby Promotions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Campaign Nearby Promotion'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="campaignNearbyPromotions view large-9 medium-8 columns content">
    <h3><?= h($campaignNearbyPromotion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Uuid') ?></th>
            <td><?= h($campaignNearbyPromotion->uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $campaignNearbyPromotion->has('company') ? $this->Html->link($campaignNearbyPromotion->company->name, ['controller' => 'Companies', 'action' => 'view', $campaignNearbyPromotion->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Uuid') ?></th>
            <td><?= h($campaignNearbyPromotion->company_uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Promotion Name') ?></th>
            <td><?= h($campaignNearbyPromotion->promotion_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($campaignNearbyPromotion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($campaignNearbyPromotion->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($campaignNearbyPromotion->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($campaignNearbyPromotion->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Images') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->images)); ?>
    </div>
    <div class="row">
        <h4><?= __('Business Locations') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->business_locations)); ?>
    </div>
    <div class="row">
        <h4><?= __('Remarks') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->remarks)); ?>
    </div>
    <div class="row">
        <h4><?= __('Launched') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->launched)); ?>
    </div>
    <div class="row">
        <h4><?= __('Active') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->active)); ?>
    </div>
    <div class="row">
        <h4><?= __('Deleted') ?></h4>
        <?= $this->Text->autoParagraph(h($campaignNearbyPromotion->deleted)); ?>
    </div>
</div>
