<?php $this->assign('title', 'Flash Nearby Promotions')?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">&nbsp;</h3>
        <?= $this->Html->link(__('Add Flash Nearby Promotion'), ['action' => 'add', $companyUuid], ['class'=>"btn btn-info"]) ?>
    </div>
    <!-- /.box-header -->

    <div class="box-body">

        <table id="dynamic-table-dept" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Promotion Name</th>
                    <th>Location Launch</th>
                    <th>Timing</th> 
                    <th class="center" width="50px" class="no-sort">Action</th>
                    
                </tr>
                </thead>
            
            <tbody>
            
            <?php foreach ($campaignNearbyPromotions as $campaignNearbyPromotion): ?>
            <tr>
                <td><?= $campaignNearbyPromotion->created->i18nFormat('dd-MM-YYYY'); ?></td>
                <td><?= $campaignNearbyPromotion->promotion_name ?></td>
                
                <?php if ($campaignNearbyPromotion->launch_type=='1'): ?>
                    <td>
                        <?php if (isset($campaignNearbyPromotion->business_locations) && trim($campaignNearbyPromotion->business_locations)!=="" && trim($campaignNearbyPromotion->business_locations)!=="null"): ?>
                        <u>Business Location :</u><br>
                        <?php foreach (json_decode($campaignNearbyPromotion->business_locations) as $idx=>$loc): ?>
                            &nbsp;<?= h((isset($list[$loc]) ? $list[$loc] : "")) ?><br/>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php else: ?>
                    <td>
                        <?php 
                            if (($campaignNearbyPromotion->business_groups) && trim($campaignNearbyPromotion->business_groups)!=="" && trim($campaignNearbyPromotion->business_groups)!=="null"): 
                                $b_group=json_decode($campaignNearbyPromotion->business_groups); 
                            ?>
                                <u>Business Group :</u><br>
                                <?php foreach ($b_group as $g_id): ?>
                                    &nbsp;<?= $grp[$g_id]['grp_name']; ?><br>
                                    <!--
                                    <u><?= $grp[$g_id]['grp_name']; ?></u><br/>
                                    <?= $grp[$g_id]['business_locations']; ?>
                                    <br/>-->
                        <?php 
                                endforeach; 
                                endif;
                            ?>
                    </td>
                <?php endif; ?>
                
                <?php if ($campaignNearbyPromotion->launched=='0'): ?>
                     <td>
                        <?php $t=json_decode($campaignNearbyPromotion->promo_time); ?>
                        <?= $t[0] ?><br/>To:<br/><?= $t[1] ?>
                        
                     </td>
                <?php else: ?>
                    <td>Operating Hours</td>
                <?php endif; ?>

                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $campaignNearbyPromotion->id, $companyUuid], ['class'=>"btn btn-block btn-xs btn-info"]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $campaignNearbyPromotion->id, $companyUuid], ['confirm' => __('Are you sure you want to delete {0}?', $campaignNearbyPromotion->promotion_name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
