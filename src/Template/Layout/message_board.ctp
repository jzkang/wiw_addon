<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>foodtriPH | <?= $this->fetch('title') ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/bootstrap/css/bootstrap.min')?>
  
  <?= $this->Html->css('foodtriph')?>
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  
  <!-- Theme style -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/dist/css/AdminLTE.min.css')?>
  
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/dist/css/skins/_all-skins.min.css')?>
  
  <!-- iCheck -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/iCheck/flat/blue.css')?>
  
  <!-- Morris chart -->
  <?=$this->Html->css('/vendor/AdminLTE-2.3.6/plugins/morris/morris.css')?>  
  
  <!-- jvectormap -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>  
  
  <!-- Date Picker -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/datepicker/datepicker3.css')?>  
  
  <!-- Daterange picker -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/daterangepicker/daterangepicker.css')?>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>
  
  <!-- bootstrap timepicker -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/timepicker/bootstrap-timepicker.min')?>  

  <!-- DataTables -->
  <?= $this->Html->css('/vendor/AdminLTE-2.3.6/plugins/datatables/dataTables.bootstrap')?>
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
	<!-- jQuery 2.2.3 -->
	<?= $this->Html->script('/vendor/AdminLTE-2.3.6/plugins/jQuery/jquery-2.2.3.min.js') ?>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>  
  
  
	<style>
		.login-logo {
			
		}
	</style>
</head>
<body>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-box">
  				<div class="login-logo">
  					<img class="" width="50" src="<?=$this->request->webroot?>img/foodtri.ph_app_icon_img.png" />		
    				<a href="http://www.foodtri.ph" target="_blank"><b>foodtri</b>PH</a><br>
    				<font size="5">~ food at your fingertips ~</font>
  				</div>
			</div>			
			<div class="box box-success">
            	<div class="box-header with-border ">
              		<i class="fa fa-bullhorn"></i>
              		<h3 class="box-title">Message Board</h3>
            	</div>
            	
            	<!-- /.box-header -->
            	<div class="box-body">
            	<?= $this->Flash->render() ?>
            	</div>
            <!-- /.box-body -->
          </div>
		</div>
	</div>

</body>
</html>