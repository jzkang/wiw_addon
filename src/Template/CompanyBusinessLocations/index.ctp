<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Company Business Location'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="companyBusinessLocations index large-9 medium-8 columns content">
    <h3><?= __('Company Business Locations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('uuid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_uuid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('business_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_business_category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mall_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ccc_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('latitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('longitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pin_latitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pin_longitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('unit_num') ?></th>
                <th scope="col"><?= $this->Paginator->sort('landmarks') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_person') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_num') ?></th>
                <th scope="col"><?= $this->Paginator->sort('currency') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($companyBusinessLocations as $companyBusinessLocation): ?>
            <tr>
                <td><?= $this->Number->format($companyBusinessLocation->id) ?></td>
                <td><?= h($companyBusinessLocation->uuid) ?></td>
                <td><?= $companyBusinessLocation->has('company') ? $this->Html->link($companyBusinessLocation->company->name, ['controller' => 'Companies', 'action' => 'view', $companyBusinessLocation->company->id]) : '' ?></td>
                <td><?= h($companyBusinessLocation->company_uuid) ?></td>
                <td><?= h($companyBusinessLocation->business_name) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->company_business_category_id) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->mall_id) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->ccc_id) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->latitude) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->longitude) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->pin_latitude) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->pin_longitude) ?></td>
                <td><?= h($companyBusinessLocation->address1) ?></td>
                <td><?= h($companyBusinessLocation->address2) ?></td>
                <td><?= h($companyBusinessLocation->street) ?></td>
                <td><?= h($companyBusinessLocation->city) ?></td>
                <td><?= h($companyBusinessLocation->state) ?></td>
                <td><?= h($companyBusinessLocation->country) ?></td>
                <td><?= h($companyBusinessLocation->country_code) ?></td>
                <td><?= h($companyBusinessLocation->postal_code) ?></td>
                <td><?= h($companyBusinessLocation->unit_num) ?></td>
                <td><?= h($companyBusinessLocation->landmarks) ?></td>
                <td><?= h($companyBusinessLocation->contact_person) ?></td>
                <td><?= h($companyBusinessLocation->contact_email) ?></td>
                <td><?= h($companyBusinessLocation->contact_num) ?></td>
                <td><?= h($companyBusinessLocation->currency) ?></td>
                <td><?= h($companyBusinessLocation->created) ?></td>
                <td><?= h($companyBusinessLocation->modified) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->created_by) ?></td>
                <td><?= $this->Number->format($companyBusinessLocation->modified_by) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $companyBusinessLocation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $companyBusinessLocation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $companyBusinessLocation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $companyBusinessLocation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
