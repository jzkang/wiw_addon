<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Company Business Location'), ['action' => 'edit', $companyBusinessLocation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Company Business Location'), ['action' => 'delete', $companyBusinessLocation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $companyBusinessLocation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Company Business Locations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company Business Location'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="companyBusinessLocations view large-9 medium-8 columns content">
    <h3><?= h($companyBusinessLocation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Uuid') ?></th>
            <td><?= h($companyBusinessLocation->uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $companyBusinessLocation->has('company') ? $this->Html->link($companyBusinessLocation->company->name, ['controller' => 'Companies', 'action' => 'view', $companyBusinessLocation->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Uuid') ?></th>
            <td><?= h($companyBusinessLocation->company_uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Business Name') ?></th>
            <td><?= h($companyBusinessLocation->business_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address1') ?></th>
            <td><?= h($companyBusinessLocation->address1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address2') ?></th>
            <td><?= h($companyBusinessLocation->address2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Street') ?></th>
            <td><?= h($companyBusinessLocation->street) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($companyBusinessLocation->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= h($companyBusinessLocation->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= h($companyBusinessLocation->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country Code') ?></th>
            <td><?= h($companyBusinessLocation->country_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postal Code') ?></th>
            <td><?= h($companyBusinessLocation->postal_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Unit Num') ?></th>
            <td><?= h($companyBusinessLocation->unit_num) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Landmarks') ?></th>
            <td><?= h($companyBusinessLocation->landmarks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Person') ?></th>
            <td><?= h($companyBusinessLocation->contact_person) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Email') ?></th>
            <td><?= h($companyBusinessLocation->contact_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Num') ?></th>
            <td><?= h($companyBusinessLocation->contact_num) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Currency') ?></th>
            <td><?= h($companyBusinessLocation->currency) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Business Category Id') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->company_business_category_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mall Id') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->mall_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ccc Id') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->ccc_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitude') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->latitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longitude') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->longitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin Latitude') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->pin_latitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin Longitude') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->pin_longitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($companyBusinessLocation->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($companyBusinessLocation->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($companyBusinessLocation->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Company Business Sub Categories') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->company_business_sub_categories)); ?>
    </div>
    <div class="row">
        <h4><?= __('Logo') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->logo)); ?>
    </div>
    <div class="row">
        <h4><?= __('Is Company Logo') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->is_company_logo)); ?>
    </div>
    <div class="row">
        <h4><?= __('Place Id') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->place_id)); ?>
    </div>
    <div class="row">
        <h4><?= __('Is Company Contact') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->is_company_contact)); ?>
    </div>
    <div class="row">
        <h4><?= __('Operating Hours') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->operating_hours)); ?>
    </div>
    <div class="row">
        <h4><?= __('Status') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->status)); ?>
    </div>
    <div class="row">
        <h4><?= __('Deleted') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessLocation->deleted)); ?>
    </div>
</div>
