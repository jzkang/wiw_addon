<?php 
use Cake\Routing\Router; 
$this->assign('title', $title);
//debug($menuAddOns);
?>
<div class="row">	
	<div class="col-md-12">
		<a href="<?=Router::url(['controller'=>'menuAddOns','action'=>'add',$vendorUuid]) ?>" class="btn bg-olive btn-flat col-md-2 col-xs-12" data-toggle="modal" data-target="#modalNewMenuAddOns" id="btnNewMenuAddOns" vendor-uuid="<?=$vendorUuid?>">
			Create Addons
		</a>
	</div>
</div>
<div class="row"><div class="col-md-12">&nbsp;</div></div>
<?= $this->element('modals',  ['id'=>'modalNewMenuAddOns','modalTitle'=>'Add New Addons','size'=>'modal-lg'])?>

<?php $arrAction = ['showEdit'=>true, 'showDelete'=>true, 'showCheckBox'=>false];?>
<?= $this->element('vendor_menu_addons', ['arrMenuAddOns' => $menuAddOns, 'arrAction' => $arrAction]) ?>