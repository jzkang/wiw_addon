<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
           
		    <?= $this->Form->create($menuAddOn, ['class'=>"form-horizontal"]) ?>
		    <?=$this->Form->hidden('vendor_id',['value'=>$vendor->id])?>
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        					<h4 class="modal-title" id="myModalLabel">Create Addons</h4>
      				</div>		    
		    		<?php if(!$parentMenuAddOns->all()->count()):?>		    		
		    		<div class="box-body">
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Create category name first</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('add_on_name',['label'=>false,'class'=>"form-control",'required'=>true, 'placeholder'=>'e.g. Drinks or Toppings or Sauce or Wrappings']);?>
	                  		</div>
                		</div>
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Description</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('description', ['label'=>false,'type'=>'text','class'=>'form-control']);?>
	                  		</div>
                		</div>		    		
		    		</div>
		    		<?php else:?>
					<div class="box-body">
						<div class="form-group">
							<label for="inputType1" class="col-sm-3 control-label">Addon Category</label>
							<div class="col-sm-9">
								<?php 
									echo $this->Form->input('parent_id', [
											'type'=>'select',
											'label'=>false,
											'class'=>'form-control select2',
											'style'=>"width: 100%;",
											'empty' => 'Select addon category',
											'default' => '',
											'options'=> $parentMenuAddOns,
											//'required' => true
										]
									);
								?>
								<small class="help-block"><i class="icon fa fa-info"></i> &nbsp;NOTE: Don't select a Category if you want to create a new Category Name</small>
							</div>
						</div>					
					
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Ref.</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('ref',['label'=>false,'class'=>"form-control",'required'=>false, 'placeholder'=>'Use this if you have your own product reference number']);?>
	                  		</div>
                		</div>					
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Name</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('add_on_name',['label'=>false,'class'=>"form-control",'required'=>true]);?>
	                  		</div>
                		</div>
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Description</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('description', ['label'=>false,'type'=>'text','class'=>'form-control']);?>
	                  		</div>
                		</div>                		
						<div class="form-group">                	
							<label for="inputName1" class="col-sm-3 control-label">Price (<?=$vendor->currency?>)</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('price', ['label'=>false,'type'=>'number','class'=>'form-control', 'value' => 0]);?>
	                  		</div>
                		</div>                		
   
                		
              		</div>
              		<?php endif;?>
              		<!-- /.box-body -->
	              <div class="box-footer">
	                <button type="reset" class="btn btn-default">Reset</button>
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right']) ?>
	                <?= $this->Form->end() ?>
	              </div>
				<!-- /.box-footer -->
          </div>	
	</div>
</div>