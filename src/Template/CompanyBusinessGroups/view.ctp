<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Company Business Group'), ['action' => 'edit', $companyBusinessGroup->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Company Business Group'), ['action' => 'delete', $companyBusinessGroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $companyBusinessGroup->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Company Business Groups'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company Business Group'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="companyBusinessGroups view large-9 medium-8 columns content">
    <h3><?= h($companyBusinessGroup->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $companyBusinessGroup->has('company') ? $this->Html->link($companyBusinessGroup->company->name, ['controller' => 'Companies', 'action' => 'view', $companyBusinessGroup->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Uuid') ?></th>
            <td><?= h($companyBusinessGroup->company_uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Group Name') ?></th>
            <td><?= h($companyBusinessGroup->group_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($companyBusinessGroup->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($companyBusinessGroup->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($companyBusinessGroup->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($companyBusinessGroup->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Business Location') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessGroup->business_location)); ?>
    </div>
    <div class="row">
        <h4><?= __('Active') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessGroup->active)); ?>
    </div>
    <div class="row">
        <h4><?= __('Deleted') ?></h4>
        <?= $this->Text->autoParagraph(h($companyBusinessGroup->deleted)); ?>
    </div>
</div>
