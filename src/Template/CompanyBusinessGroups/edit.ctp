<?php $this->assign('title', 'Edit Business Group')?>
<div class="row">
    <div class="col-md-12 form-group">
        <section class="panel">
            <div class="panel-body">
                <?= $this->Form->create($companyBusinessGroup,['class'=>"form-horizontal",'url'=>['controller'=>'companyBusinessGroups','action'=>$action ,$companyBusinessGroup[0]["id"],$companyUuid]]) ?>
                     <?php 
                        //Hidden Fields
                        echo $this->Form->hidden('id',['value'=>$companyBusinessGroup[0]["id"]]);
                        echo $this->Form->hidden('companyUuid',['value'=>$companyUuid]);
                    ?>    
                    <div class="form-group">
                    <label class="col-sm-2">Group Name</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input("group_name", ['label'=>false, 'type'=>'text', 'value'=>@$companyBusinessGroup[0]["group_name"], 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"45", 'required'=>"required"]);?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="col-sm-2">Description</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input("description", ['label'=>false, 'type'=>'text', 'value'=>@$companyBusinessGroup[0]["description"], 'class'=>'form-control','placeholder'=>"", 'maxlength'=>"45", 'required'=>"required"]);?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2">Select Business Location</label>
                        <div class="col-sm-10">
                        <?= 
                            /*
                            $options = [
                                'Value 1' => 'Label 1',
                                'Value 2' => 'Label 2'
                            ];*/
                            $this->Form->select('bu', $list, [
                                'multiple' => 'checkbox',
                                'default'=> json_decode($companyBusinessGroup[0]["business_location"],TRUE)
                            ]);

                        ?>
                        </div>
                    </div>

                    <br style="clear: both;">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <?= $this->Form->button(__('Save'), ['class'=>"btn btn-info"]) ?>
                            <?= $this->Html->link(__('Back'), ['action' => 'index',$companyUuid], ['class'=>"btn btn-info"]) ?>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
    </div>
</div>