<?php $this->assign('title', 'Business Groups')?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">&nbsp;</h3>
        <?= $this->Html->link(__('Add Business Group'), ['action' => 'add', $companyUuid], ['class'=>"btn btn-info"]) ?>
    </div>
    <!-- /.box-header -->

    <div class="box-body">

        
        <table id="dynamic-table-dept" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Group Name</th>
                <th>Business Locations</th>
                <th class="center">Action</th>
            </tr>
            </thead>
            
            <tbody>
            
            <?php foreach ($companyBusinessGroups as $companyBusinessGroup): ?>
            <tr>
                <td><?= h($companyBusinessGroup['group_name']) ?></td>
                <td>
                <?php foreach (json_decode($companyBusinessGroup->business_location) as $idx=>$loc): ?>
                <?= h((isset($list[$loc]) ? $list[$loc] : "")) ?><br/>
                <?php endforeach; ?>
                </td>

                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $companyBusinessGroup->id, $companyUuid], ['class'=>"btn btn-block btn-xs btn-info"]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $companyBusinessGroup->id, $companyUuid], ['confirm' => __('Are you sure you want to delete {0}?', $companyBusinessGroup->group_name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
