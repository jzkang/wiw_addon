<?php if (!empty($vendorAddress->operating_hours)): ?>
<?php
	$arrOperatingHours = json_decode(($vendorAddress->operating_hours), true );
?>

<div class="box box-primary">
	<div class="box-body box-profile">

		<!-- <h3 class="profile-username text-center"><?= $vendorAddress->address_name;?><br><small>( <?=$vendorAddress->uuid?> )</small></h3>  -->
		<h3 class="profile-username">Your Operations</small></h3>
		
		<ul class="list-group list-group-unbordered">

			<li class="list-group-item">
				<strong><i class="fa fa fa-clock-o margin-r-5"></i> <b>Meetup Timing and Location</b></strong> 
              	<p class="text-muted">
					<?php 
					foreach ($arrOperatingHours as $v) {
						if (key($v) == 'freetext') {
							echo "<span>Others: <strong>$v[freetext]</strong></span><br>";
							continue;	
						}
						$strDay = key($v);
						$strOperate = "<strong>".ucfirst($strDay)."</strong>" . '&#09;' . $v[$strDay]['start'] . ' - ' . $v[$strDay]['end'] . ' @ ' . @$v[$strDay]['loc'];
						echo "<span>$strOperate</span><br>";
					}					
					?>
              	</p>
				<?= $this->Html->link(__('Edit'), ['controller'=>'vendorAddresses', 'action' => 'editOp', $vendorAddress->vendor_uuid],['class'=>'label label-warning btnEditVendOperation', 'data-toggle'=>"modal", 'data-target'=>"#modalEditVendOperation", 'id'=>"btnEditVendOperation",'vendor-uuid'=>$vendorAddress->uuid,'branch-id'=>$vendorAddress->id] ) ?>				
			</li>	
		</ul>
	</div>
</div>
<?php endif;?>
<?= $this->element('modals',  ['id'=>'modalEditVendOperation','modalTitle'=>'Edit Operation Details','size'=>'modal-lg'])?>

	