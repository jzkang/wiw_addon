<table style="width:100%; border: 1px solid lightgray; border-collapse: collapse;" cellpadding="10" >
    <thead>
        <tr>
            <td align="center" style="border: 1px solid lightgray;">#</td>
            <td style="border: 1px solid lightgray;" colspan=2><b>&nbsp;Item Details</b></td>
            <td style="border: 1px solid lightgray;" align="center"><b>Price (<?=$strCurrency?>)</b></td>
            <td style="border: 1px solid lightgray;" align="center"><b>Qty</b></td>
            <td style="border: 1px solid lightgray;" align="center"><b>Total (<?=$strCurrency?>)</b></td>
        </tr>
    </thead>
    <tbody>
    
    <?php if (!empty($arrItems)): $intSN=0;?>       
    <?php $isSSL = empty($_SERVER['HTTPS']) ? 'http://' : 'https://';?>         
    <?php foreach($arrItems as $v): ?>
    <?php 
        $strAddOns = '';
        if (!empty($v->add_ons)) {

            $arrItemAddOns = $v->add_ons;
            if (!is_array($v->add_ons))
                $arrItemAddOns = json_decode($v->add_ons, true);
                
            if (!empty($arrItemAddOns)) {
                foreach ($arrItemAddOns as $arrAddOns) {
                    $strAddOns .=  '&nbsp; - &nbsp;'.@$arrAddOns['add_on_name'] . ' - ('.$strCurrency.') &nbsp;' . number_format($arrAddOns['price'],2)  . '  x ' . $arrAddOns['qty'] . '<br>';
                }
            }
        }
        
        //menu photo
        $arrMenuPhoto = json_decode($v->menu->photo, true);
        $strMenuPoto = $isSSL . $this->request->host() . $this->request->webroot . $arrMenuPhoto['path'].DS.$arrMenuPhoto['name'];

        $v->price = number_format($v->price, 2);
        $v->total_amount = number_format($v->total_amount, 2);
    ?>
    <tr>
        <td style="border: 1px solid lightgray;" align="center"><?=++$intSN?></td>
        <td style="border: 1px solid lightgray; border-right:0px !important; width:80px" align="center">
        	<img alt="" src="<?=$strMenuPoto?>" width="75px">
        </td>
        <td style="border: 1px solid lightgray;">            
            <h3>&nbsp;<?=$v->menu_name?></h3>
            <p><?=$strAddOns?></p>
        </td>
        <td style="border: 1px solid lightgray;" align="right"><?=$v->price?>&nbsp;</td>
        <td style="border: 1px solid lightgray;" align="center"><?=$v->qty?>&nbsp;</td>
        <td style="border: 1px solid lightgray;" align="right"><?=$v->total_amount?>&nbsp;</td>
    </tr>         
    <?php endforeach;?>       
    <?php endif;?>                    
        <tr>
            <td style="border: 1px solid lightgray;" colspan="5" align="right">Sub Total &nbsp;</td>
            <td style="border: 1px solid lightgray;" align="right"><strong><?=$strSubTotal?>&nbsp;</strong></td>
        </tr> 
        <tr>
            <td style="border: 1px solid lightgray;" colspan="5" align="right">Discounts&nbsp;</td>
            <td style="border: 1px solid lightgray;" align="right"><strong>0.00</strong></td>
        </tr>
        <!--
        <tr>
            <td style="border: 1px solid lightgray;" colspan="5" align="right">Delivery&nbsp;</td>
            <td style="border: 1px solid lightgray;" align="right"><strong>0.00</strong></td>
        </tr>                                                 
        -->
        <tr>
            <td style="border: 1px solid lightgray;" colspan="5" align="right">Total &nbsp;</td>
            <td style="border: 1px solid lightgray;" align="right"><strong><?=$strSubTotal?>&nbsp;</strong></td>
        </tr>                                    
    </tbody>
</table> 
