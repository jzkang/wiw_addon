<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="message success" onclick="this.classList.add('hidden')"><?//= $message ?></div> -->

<div class="row">
	<div class="col-md-12">
	
		<div class="callout callout-info">
			<h4><i class="icon fa fa-info"></i>&nbsp;&nbsp; Did you know?</h4>
			<p><?= $message ?></p>
		</div>	
	</div>
</div>