<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="message error" onclick="this.classList.add('hidden');"><?//= $message ?></div>  -->
<div class="row">
	<div class="col-md-12">
		<div class="message error alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-ban"></i>&nbsp;&nbsp; Oops! Something went wrong.</h4>
			<?= $message ?>
		</div>
	</div>
</div>
