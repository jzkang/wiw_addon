
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title pull-left"><?=__('Add Your Company')?></h3>   
		<p class="pull-right ">
			<i class="fa fa fa-map-marker margin-r-5"></i><span id="geoloc" ></span>
		</p>         	
    </div>
	<div class="box-body with-border">
		<?= $this->Form->create($company,array('class'=>"form-horizontal")) ?>
		<input type="hidden" name="latlong" id="latlong">
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->select('country_code',$Country, ['class'=>'form-control', 'empty'=>__('Choose your country of business...'), 'required'=>true ]);?>
	                  		</div>
                		</div>
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->select('type',$BusinessType, ['class'=>'form-control', 'empty'=>__('Type of business...'), 'required'=>true ]);?>
	                  		</div>
                		</div>                		
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->input('company_name',array('label'=>false,'class'=>"form-control",'id'=>'inputName1','required'=>true, 'placeholder' => __('Company Name'), 'maxlength'=>'50'));?>
	                  		</div>
                		</div>
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->input('contact_person',array('label'=>false,'class'=>"form-control",'id'=>'inputFullName1','required'=>true, 'placeholder' => __('Contact Person'), 'maxlength'=>'50'));?>
	                  		</div>
                		</div>
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">         		
                    			<?=  $this->Form->input('contact_email',array('type'=>'email', 'label'=>false,'class'=>"form-control",'id'=>'inputEmail1','required'=>true, 'placeholder' => __('Company Email') ));?>
                  			</div>
                		</div>
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">
                    			<?=  $this->Form->input('contact_num',array('type'=>'phone', 'label'=>false,'class'=>"form-control",'id'=>'inputContactNum1','required'=>true, 'placeholder' => __('Contact Number')));?>
                  			</div>
                		</div>           
                		<!-- 
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">
                    			<?=  $this->Form->input('url',array('type'=>'url', 'label'=>false,'class'=>"form-control",'id'=>'inputUrl1','placeholder'=> __('Website')));?>
                  			</div>
                		</div>
                		 -->                		     		
                		<div class="form-group">
                			<div class="col-sm-12 col-md-12">
								<div class="checkbox icheck" style="padding-left: 20px;">
		            				<label>
		              					<input required=true type="checkbox">&nbsp;&nbsp;<?=__('I agree to the')?>  <a href="#" target="_blank"><?=__('terms')?></a>
		            				</label>
		          				</div>
	          				</div>
          				</div>
                		
                		<!-- reCaptcha -->                		
                      	<div class="form-group">
                  			<div class="col-sm-12 col-md-12">                    		
                    			<div class="g-recaptcha" data-sitekey="6Ld9RRgUAAAAAEjl99aPJUewJs0LC6SB9lpNBgdC"></div>
                  			</div>
                		</div>        		
              			
              			
              		<!-- /.box-body -->
	              <div class="box-footer">
	              <div class="col-sm-12 col-md-12" style="padding-left: 0px;">                    		
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
	                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary ']) ?>
	                <?= $this->Form->end() ?>
	                <div class="pull-right"><span><?=__('Click here to <a href="'.$defaultLoginUrl.'"><u>login</u></a> to your compay account.')?> </span></div>
					</div>
	              </div>
				<!-- /.box-footer -->	
	</div>
</div>
<script>

var x = document.getElementById("geoloc");

if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(showPosition);
} else { 
	x.innerHTML = "Geolocation is not supported by this browser.";
}

function showPosition(position) {
    $("#latlong").val(position.coords.latitude + ',' + position.coords.longitude)
    x.innerHTML = "Lat: " + position.coords.latitude + " | Long: " + position.coords.longitude;

    //check if country is allowed
    $.ajax({
		url: "<?=$this->request->webroot?>companies/verifyLocation/" + position.coords.latitude + ',' + position.coords.longitude,
		cache: false
	}).done(function( html ) {
		//$( "#geoloc" ).append( html );
		x.innerHTML = "<?=__('Your currently in')?> <strong>"+html+"</strong>";
		
	});
    
}
</script>
