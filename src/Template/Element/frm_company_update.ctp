<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title pull-left"><?=__('Update Your Company')?></h3>   
    </div>
	<div class="box-body with-border">
		<?= $this->Form->create($company,array('class'=>"form-horizontal")) ?>
		<input type="hidden" name="latlong" id="latlong">
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->select('country_code',$Country, ['class'=>'form-control', 'empty'=>__('Choose your country of business...'), 'required'=>true ]);?>
	                  		</div>
                		</div>
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->select('type',$BusinessType, ['class'=>'form-control', 'empty'=>__('Type of business...'), 'required'=>true ]);?>
	                  		</div>
                		</div>                		
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->input('company_name',array('label'=>false,'class'=>"form-control",'id'=>'inputName1','required'=>true, 'placeholder' => __('Company Name'), 'maxlength'=>'50'));?>
	                  		</div>
                		</div>
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->input('contact_person',array('label'=>false,'class'=>"form-control",'id'=>'inputFullName1','required'=>true, 'placeholder' => __('Contact Person'), 'maxlength'=>'50'));?>
	                  		</div>
                		</div>
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">         		
                    			<?=  $this->Form->input('contact_email',array('type'=>'email', 'label'=>false,'class'=>"form-control",'id'=>'inputEmail1','required'=>true, 'placeholder' => __('Company Email') ));?>
                  			</div>
                		</div>
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">
                    			<?=  $this->Form->input('contact_num',array('type'=>'phone', 'label'=>false,'class'=>"form-control",'id'=>'inputContactNum1','required'=>true, 'placeholder' => __('Contact Number')));?>
                  			</div>
                		</div>           
						<div class="form-group">                						
							<div class="col-sm-12 col-md-12">
	                    		<?=  $this->Form->input('description_long',array('type'=>'textarea', 'label'=>false,'class'=>"form-control",'id'=>'inputName1','required'=>true, 'placeholder' => __('Company Description')));?>
	                  		</div>
                		</div>                		
                		<div class="form-group">
							<div class="col-sm-12 col-md-12">
                    			<?=  $this->Form->input('url',array('type'=>'url', 'label'=>false,'class'=>"form-control",'id'=>'inputUrl1','placeholder'=> __('Website')));?>
                  			</div>
                		</div>   		     		
              		<!-- /.box-body -->
              		
	              <div class="box-footer">
	              <div class="col-sm-12 col-md-12" style="padding-left: 0px;">                    		
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
	                <?= $this->Form->button(__('Update'),['class'=>'btn btn-primary ']) ?>
	                <?= $this->Form->end() ?>
					</div>
	              </div>
				<!-- /.box-footer -->	
	</div>
</div>