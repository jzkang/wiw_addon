<?php 
if (empty($menu))
	$menu = new App\Model\Entity\Menu();
if (empty($action))
	$action = 'add';
?>
<?php if ('add' === $action):?>
<?= $this->Form->create($menu,['class'=>"form-horizontal",'type' => 'file', 'url'=>['controller'=>'menus','action'=>$action,$vendor->uuid]]) ?>
<?php elseif ('edit' === $action):?>
<?= $this->Form->create($menu,['class'=>"form-horizontal",'type' => 'file', 'url'=>['controller'=>'menus','action'=>$action,$vendor->uuid,$menu->id]]) ?>
<?php endif;?>
<?php 
$this->Form->hidden('vendor_id',['value'=>$vendor->id]);
$this->Form->hidden('vendor_uuid',['value'=>$vendor->uuid]);

?>
<div class="box box-primary">
	<div class="modal-header <?=(@$missingMenu) ? 'bg-red' : ''?>">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel"><?=ucfirst($action)?> Menu</h4>
	</div>
	<div class="box-body">
						<div class="form-group">                	
							<label for="inputRef" class="col-sm-3 control-label">Ref</label>					
							<div class="col-sm-9">
	                    		<?=  $this->Form->input('ref',array('label'=>false,'class'=>"form-control",'id'=>'inputRef', 'placeholder'=>'Use this if you have your own product reference number'));?>	                    		
	                  		</div>
                		</div>	
						<div class="form-group">                	
							<label for=inputName class="col-sm-3 control-label">Name</label>					
							<div class="col-sm-9">
	                    		<?= $this->Form->input('name', ['label'=>false,'class'=>'form-control','id'=>'inputName','required'=>true]);?>
	                  		</div>
                		</div>                	
                		<div class="form-group">
                  			<label for="inputCategory" class="col-sm-3 control-label">Category</label>
                  			<div class="col-sm-9">                    		
 								<input list="category" name="menu_category_name" class="form-control" id="inputCategory">
								<?php 
								$strOptionCategory = '';
								if (!empty($Category)) {
									foreach ( $Category as $k => $v ) {
										$strOptionCategory .= '<option value="'.$v.'">'; 
									}
								}
								?>
								<datalist id="category">
									<?=$strOptionCategory?>
								</datalist>                     			
                  			</div>
                		</div>
                		<div class="form-group">
                  			<label for="inputDescShort" class="col-sm-3 control-label">Short Description</label>
                  			<div class="col-sm-9">                    		
                    			<?//= $this->Form->input('description_short',array('type'=>'textarea', 'label'=>false,'class'=>"form-control",'id'=>'inputDescShort','required'=>true,'maxlength'=>'500', 'placeholder'=>'Summarize whats with this menu in 500 characters'));?>
                    			<textarea name="description_short" class="form-control" rows="3" placeholder="Summarize whats with this menu in 500 characters" maxlength='500' id='inputDescShort' required=true ><?=$menu->description_short?></textarea>
                  			</div>
                		</div>            
                		<!--     	
                		<div class="form-group">
                  			<label for="inputDescLong" class="col-sm-3 control-label">Long Description</label>
                  			<div class="col-sm-9">                    		
                    			<?//=  $this->Form->input('description_long',array('type'=>'textarea', 'label'=>false,'class'=>"form-control",'id'=>'inputDescLong','required'=>false,'maxlength'=>'500'));?>
                    			<p class="help-block">Tell us more about the menu in 500 characters</p>
                  			</div>
                		</div>
                		 -->                		
                		<div class="form-group">
                  			<label for="inputPrice" class="col-sm-3 control-label">Price (<?=$vendor->currency?>)</label>
                  			<div class="col-sm-9">         		
                    			<?=  $this->Form->input('price',array('type'=>'number', 'label'=>false,'class'=>"form-control",'id'=>'inputPrice','required'=>true, 'placeholder'=>'key-in only the amount without currency symbol'));?>
                    			<script>$('#inputPrice').on('focus',function(e){   $(this).select();});</script>
                  			</div>
                		</div>
<?php
/* 
                		<div class="form-group">
                  			<label for="inputDiscount" class="col-sm-3 control-label">Discount</label>
                  			<div class="col-sm-9">                    		
                    			<?//=  $this->Form->input('discount',array('type'=>'nubmer', 'label'=>false,'class'=>"form-control",'id'=>'inputDiscount','required'=>false));?>
                  			</div>
                		</div>
*/
?>
<?php
/*                 		
                		<div class="form-group">
                  			<label for="inputPaxMin" class="col-sm-3 control-label">Min Pax</label>
                  			<div class="col-sm-9">                    		
                    			<?//=  $this->Form->input('pax_min',array('type'=>'nubmer', 'label'=>false,'class'=>"form-control",'id'=>'inputPaxMin','required'=>false));?>
                  			</div>
                		</div>
*/?>                		                		
                		<div class="form-group">
                  			<label for="inputPaxMax" class="col-sm-3 control-label">Max Pax</label>
                  			<div class="col-sm-9">                    		
                    			<?=  $this->Form->input('pax_max',array('type'=>'number', 'label'=>false,'class'=>"form-control",'id'=>'inputPaxMax','required'=>false, 'placeholder'=>'How many person per serving?'));?>
                  			</div>
                		</div>   
                		
						<div class="form-group">
							<label for="inputPhoto" class="col-sm-3 control-label">Photo</label>
							<div class="col-sm-9">
								<div class="row">
									<div class="col-sm-8">
										<?= $this->Form->file('photo',['id'=>'inputPhoto', 'required'=> (empty($menu->photo)) ? true : false])?>
										<p class="help-block">Image size within 600x400 and .JPG or .PNG only</p>									
									</div>
									<div class="col-sm-4">
										<?php $arrPhoto = json_decode($menu->photo,true);?>									
										<img class="img-responsive pull-left" alt="Menu Image" src="<?= $this->request->webroot . $arrPhoto['path'] . DS . $arrPhoto['name']?>">
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<hr>
							<label for="inputPhoto" class="col-sm-3 control-label">Menu Add-ons</label>
							<div class="col-sm-9">							
							<?php 
								$arrAction = ['showEdit'=>false, 'showDelete'=>false, 'showCheckBox'=>true];
								echo $this->element('vendor_menu_addons',compact('arrMenuAddOns','arrAction'));
							?>
							</div>
						</div>	                		            
                		                			
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="reset" class="btn btn-default">Reset</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<?= $this->Form->button(__('&nbsp;&nbsp; Submit &nbsp;&nbsp;'),['class'=>'btn btn-primary pull-right']) ?>
	</div>
	<!-- /.box-footer -->	
</div>

<?= $this->Form->end() ?>