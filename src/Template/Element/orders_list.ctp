<?php 
//debug(count($arrOrders));
//debug($arrOrders);
?>
<?php if (count($arrOrders)):?>	
<?php foreach ($arrOrders as $order): $intItemCount=0;?>

<div class="box">
	<!-- /.box-header -->
	<div class="box-body">
		
						<div class="row">
							<div class="col-md-12">
								<div class="tab-pane active" id="timeline">
								
									<!-- The timeline -->
									<ul class="timeline timeline-inverse">
                  						<!-- timeline time label -->
                  						<li class="time-label">
                        					<span class="bg-blue">
                        						<?php 
												$date = new \DateTime();
												$date->setTimestamp(strtotime($order-> created));
												$interval = $date->diff(new \DateTime( 'now'));
												$strFormat = "";
												if ($interval->y ) $strFormat .= ($interval->y > 1) ? $interval->y . ' yrs, ' : $interval->y . ' yr, ' ;
												if ($interval->m ) $strFormat .= ($interval->m > 1) ? $interval->m .' mths, ' : $interval->m . ' mth, ' ;
												if ($interval->d ) $strFormat .= ($interval->d > 1) ? $interval->d . ' days, ' : $interval->d . ' day, ' ;
												if ($interval->h ) $strFormat .= ($interval->h > 1) ? $interval->h .' hrs, ' : $interval->h . ' hr, ' ;
												if ($interval->i ) $strFormat .= ($interval->i > 1) ? $interval->i . ' mins, ' : $interval->i . ' min, ' ;
                        						?>
                        						<h5 class="pull-left">Date: <strong><?=date('d M Y h:iA', strtotime($order->created) ) . ' ('.$interval->format(trim($strFormat,', ' )).')' ?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;</h5>
                        						<h5 class="pull-left">Order ID: <strong><?=$order->uuid?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;</h5> 
                        						<h5 class="pull-left">Total Amount: <strong><?= $defaultCurrencySymbol . ' ' . number_format($order->total_amount,2)?></strong></h5> 
<!--                         						<button type="button" class="btn  btn-success btn-sm pull-right">Acknowledge</button> -->
                        					</span>
                        					<span class="pull-right">
                        						<?php $color = ( $order->status == 1 || $order->status == 0 ) ? 'red' : 'green'?>
                        						<h4><font color="<?=$color?>"><?=$arrTransacStatus[$order->status]?></font></h4>
                        					</span>
                  						</li>
                  						<?php foreach ($order->transaction_items as $item): ?>                  
                  						<li>
                  							<i class="fa  bg-blue"><font size="2"><strong><?=++$intItemCount?></strong></font></i>
											<div class="timeline-item">
												<span class="time">Sub-Total: <strong><?= $defaultCurrencySymbol . ' ' . number_format($item->total_amount,2)?></strong> </span>
												<h3 class="timeline-header "><a><?=$item->menu_name?></a>&nbsp;&nbsp;|&nbsp;&nbsp;Qty: <strong><?=$item->qty?></strong></h3>
												<div class="timeline-body row">
	                        						<div class="col-xs-12 col-sm-3 col-md-2" style="border-right: 2px solid lightgray; margin-right: 2.5px;">
	                        							<img class="img-responsive center-block img-thumbnail" alt="Menu Image" src="<?= rtrim($this->request->webroot, '/') . $item->photo?>">
	                        						</div>	                        						
	                        						<div class="col-xs-12 col-sm-8 col-sm-8 col-md-9">
		                        						<?php if (!empty($item->add_ons)):?>
														<div class="">
		                        							<span><u>Add-ons:</u></span><br>		                        							
		                        							<ul>
		                        								<?php foreach ($item->add_ons as $addOns): ?>	                        							
		                        								<li>
		                        									<div></div>
		                        									<?= (!empty($addOns['add_on_name'])) ? $addOns['add_on_name'] : $addOns['name']?> | Qty: <strong><?=$addOns['qty']?></strong></li>
		                        								<?php endforeach;?>
		                        							</ul>														
														</div>	                        						
	                        							<?php else:?>
														<div class="callout callout-warning">
															<p>No Add-ons</p>
														</div>	                        								
	                        							<?php endif;?>
	                        						</div>

	                      						</div>
	                    					</div>
                  						</li>
                  						<?php endforeach;?>
										<li class="time-label">
											<i class="fa fa-clock-o bg-gray"></i>
										</li>

									</ul>	
									
									<div class="row">
										<div class="col-md-6">
											<div class="alert alert-info alert-dismissible">
                									<h4><i class="icon fa fa-info"></i> User Details</h4>
													<div class="user-panel">
        												<div class="pull-left image">
														<?php 
															$arrUserPhoto = json_decode($order->user->photo);
															$strUserPhoto = $defaultImg; 
															if (!empty($arrUserPhoto))
																$strUserPhoto = $apiUrl . $arrUserPhoto->path . $arrUserPhoto->name; 
														?>        												
          													<img src="<?=$strUserPhoto?>" class="img-circle" alt="User Image">
        												</div>
        												<div class="pull-left info">
          													<p><?=$order->user->first_name . ' ' . $order->user->last_name?></p>
          													<a href="tel:<?=$order->user->mobile?>"><i class="fa fa-phone "></i><?=$order->user->mobile?></a> |&nbsp; 
          													<a href="mailto:<?=$order->user->email?>"><i class="fa fa-envelope"></i><?=$order->user->email?></a>
        												</div>
      												</div>                								
								                	<span><i class="fa fa fa-map-marker  margin-r-5"></i> Meetup: <strong><?=$order->meetup?></strong></span><br>
								                	<span><i class="fa fa  fa-commenting  margin-r-5"></i> Remarks: <strong><?=$order->remarks?></strong></span>
								                
              								</div>										
										</div>
										<div class="col-md-6">
											<?php if ($order->status == 1):?>																
											<button message="Are you sure you want to accept this order?" customer="<?=$order->user->first_name?>" uuid="<?=$order->uuid?>" status="2" type="button" class="btn btn-block btn-success btn-action accept <?=$order->uuid?>">
												<i class="icon fa fa-check"></i> &nbsp;Accept 
											</button>
											<button customer="<?=$order->user->first_name?>" uuid="<?=$order->uuid?>" status="0" type="button" class="btn btn-block btn-danger btn-action <?=$order->uuid?>">
												<i class="icon fa fa-ban"></i> &nbsp;Reject
											</button>
											
											
											
											<br>
											<div class="form-group hidden" id="divReason-<?=$order->uuid?>">                	
												<label for="inputName1" class="col-sm-3 control-label">Reason</label>					
												<div class="col-sm-9">
	                    							<div class="input text"><input id="reason-<?=$order->uuid?>" placeholder="Please let your customer know why you rejected the order" name="reason" class="form-control" id="desc" type="text" requried></div>	                  		
	                    						</div>
                							</div>
											<?php elseif ($order->status == 2):?>
											<button message="Are you sure you have delivered this order?" uuid="<?=$order->uuid?>" status="5" type="button" class="btn btn-block btn-success btn-action <?=$order->uuid?>">
												<i class="icon fa fa-check"></i> &nbsp; Delivered
											</button>
											<?php endif;?>										
										</div>										
									</div>

									
								</div>
							</div>
						</div>
	</div>
	<!-- /.box-body -->
</div>
<!-- /.box -->
<?php endforeach;?>
<?php else:?>
<div class="callout callout-danger">
	<h4>No <?=$title?> Found!</h4>
</div>		
<?php endif;?>

          
<script>
//  $(function () {
//    $('#<?=$tableId?>').DataTable();
//  });

  $(document).ready(function(){
	    $(document).off('click','.btn-action');
	    $(document).on('click','.btn-action',function(){

		  	var orderStatus = $(this).attr('status');
			var orderUuid = $(this).attr('uuid');
			var customer = $(this).attr('customer');
			var messageConfirm = $(this).attr('message');

			$(this).off('click','.btn-action');
			
			if (orderStatus == 0 ) {

				bootbox.prompt({
				    title: "Please let <strong>" + customer + "</strong> know the reason!",
				    inputType: 'textarea',
	    		    buttons: {
	    		        confirm: {
	    		            label: 'Reject',
	    		            className: 'btn-danger'
	    		        },
	    		        cancel: {
	    		            label: 'Close',
	    		            className: 'btn-default'
	    		        }
	    		    },				    
				    callback: function (result) {

				    	var data = { status: orderStatus, uuid: orderUuid, reason: result };
						if (result != null) { 
		    	    		//disable button
		    	    		$("."+orderUuid).attr('disabled','disabled');
		    	    		$("."+orderUuid).html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Processing...');							
	    					$.ajax({
		  						  method: "POST",
		  						  url: "<?= $this->request->webroot ?>" + "transactions/status",
		  						  data: data,
		  						  cache: false
		  						})
		  						.done(function( msg ) {
		  							//alert( "Data Saved: " + msg );
		  							location.reload();
		  						});	
						}
				    	
				    	
				    }
				});				

			} else if (orderStatus != 0) {

				
	    		bootbox.confirm({
	    		    message: messageConfirm,
	    		    buttons: {
	    		        confirm: {
	    		            label: 'Yeah!',
	    		            className: 'btn-success'
	    		        },
	    		        cancel: {
	    		            label: 'Not now',
	    		            className: 'btn-default'
	    		        }
	    		    },
	    		    callback: function (result) {
	
	    		    	var data = { status: orderStatus, uuid: orderUuid };	    		    
		    		    if (result != false) {
		    	    		//disable button
		    	    		$("."+orderUuid).attr('disabled','disabled');
		    	    		$("."+orderUuid).html('<i class="fa fa-spin fa-refresh"></i>&nbsp; Processing...');    		    
	    					$.ajax({
		  						  method: "POST",
		  						  url: "<?= $this->request->webroot ?>" + "transactions/status",
		  						  data: data,
		  						  cache: false
		  						})
		  						.done(function( msg ) {
		  							//alert( "Data Saved: " + msg );
		  							location.reload();
		  						});				    		    
			    		}
	    		        
	    		    }
	    		});

		    } 

		});
	});
</script>          