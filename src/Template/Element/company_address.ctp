    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
<?php if ( !empty($company) ): ?>
<div class="box box-primary">
	<div class="box-body box-profile">

		<h3 class="profile-username">
			<i class="fa fa fa-map  margin-r-5"></i>
			<?=__('Company Address')?>
		</h3>
		
		<ul class="list-group list-group-unbordered">
        	<li class="list-group-item">
        		<div class="row">
					<div class="col-md-4">
		            	<strong>
		            		<i class="fa fa fa-map-marker  margin-r-5"></i>
		            		<a target="_blank" href="http://maps.google.com/?daddr=<?=$company->latitude.','.$company->longitude?>"> Map</a>
		            	</strong>
		            	<span>
			       			<p class="text-muted">
			       				<?= empty($company->address1) ? "" : h($company->address1) . ", "?>
			       				<?= empty($company->address2) ? "" : h($company->address2) ?>
			       				<?= empty($company->street) ? "" : " ".h($company->street) ?>
			       				<?= empty($company->city) ? "" : "<br>".h($company->city) ?>
			       				<?= empty($company->state) ? "" : "<br>".h($company->state) ?>
			       				<?= empty($company->country_code) ? "" : "<br>".h($Country[$company->country_code]) ?>
			       				<?= empty($company->post_code) ? "" : "<br>".h($company->post_code) ?>
				                <?= empty($company->landmarks) ? "" : "<br><u>Landmark</u>: ".h($company->landmarks) ?>
			       			</p>
		            	</span>
						<?= $this->Html->link(__('Edit Address'), ['controller'=>'companies', 'action' => 'editAddress', $company->uuid],['class'=>'label label-warning btnEditCompanyAddress', 'data-toggle'=>"modal", 'data-target'=>"#modalEditCompanyAddress", 'id'=>"btnEditCompanyAddress",'company-uuid'=>$company->uuid,] ) ?>					
					</div>
					<div class="col-md-8">
    <div id="map"></div>
    <script type="text/javascript">
      function initMap() {
        var uluru = {lat: <?=$company->latitude?>, lng: <?=$company->longitude?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom:15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9dQz2vPKrVevVsQPy4QP-wCJXO_5cCtY&callback=initMap"></script>
					</div>				            	
			</li>
		</ul>
	</div>
</div>
<?php endif;?>
<?= $this->element('modals',  ['id'=>'modalEditCompanyAddress','modalTitle'=> __('Update Company Address'),'size'=>'modal-lg'])?>