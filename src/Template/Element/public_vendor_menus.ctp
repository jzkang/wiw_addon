<?php if (!empty($arrVendorMenus)): $counter = 0; $intDivGrid = 6; $imgHeightLimit = null;?>
<?php foreach ($arrVendorMenus as $menus): $counter++; ?>
<?php ob_start(); ?>
	<div class="col-md-<?=$intDivGrid?>">
		<div class="box box-primary">
			<div class="box-header with-border">
            	<h3 class="box-title pull-left"><a href="<?=$this->request->webroot . 'provider/' . $provider . DS . 'menus/' . $menus->id ?>" ><?= h($menus->name) ?></a></h3>
            </div>
            <!-- /.box-header -->
            		
			<div class="box-body row border-between">
				<div class="col-md-12">
					<?php 
						$strImgSrc = $defaultImg;
						if(!empty($menus->photo)) {
							$arrPhoto = json_decode($menus->photo,  true);
							$strImgSrc =  $this->request->webroot . $arrPhoto['path'] . DS . $arrPhoto['name'];
							if (empty($imgHeightLimit))
								$imgHeightLimit = $arrPhoto['dimensions']['height'];
							elseif ($imgHeightLimit > $arrPhoto['dimensions']['height'])
								$imgHeightLimit = $arrPhoto['dimensions']['height'];
								
						}
					?>
					<a href="<?=$this->request->webroot . 'provider/' . $provider . DS . 'menus/' . $menus->id ?>" > 
						<img class="img-responsive center-block img-thumbnail" alt="{{Menu Image}}" src="<?= $strImgSrc?>">
					</a>
				</div>
				<div class="col-md-12">
				
					<h3 class="profile-username text-center">
						<font color="red"><strong><?= $defaultCurrencySymbol . ' ' . number_format($menus->price, 2) ;?></strong></font>
						<?php if ($menus->pax_max):?>
						<small>for <?= $menus->pax_max ?> pax</small> 
						<?php endif;?>
					</h3>

				</div>				
				<div class="col-md-12">

	          		<div class="box box-solid">
	
	              		<div class="box-group" id="accordion_<?=$counter?>">
        		
	                		<div class="panel box box-primary">
	                  			<div class="box-header with-border">
				                    <h4 class="box-title">
	            						<a data-toggle="collapse" data-parent="#accordion_<?=$counter?>" href="#collapseTwo_<?=$counter?>" class="collapsed" aria-expanded="false">
	                        				Menu Add-Ons
	                      				</a>
	                    			</h4>
	                  			</div>
	                  			<div id="collapseTwo_<?=$counter?>" class="panel-collapse " aria-expanded="false" style="">
									<?php $arrAddOns = json_decode($menus->add_ons, true);?>
									<?php if (!empty($arrAddOns)):?>	    
									<?php foreach ($arrAddOns as $k => $v):?>
	                  				<div class="box-header with-border">
              							<i class="fa fa-th-large"></i>
              							<h3 class="box-title"><?=$v['add_on_name']?></h3>
            						</div>
            						<div class="box-body">
            							<?php if (!empty($v['items'])):?>
            							<ul>
											<?php foreach ($v['items'] as $item):?>
												<li>
													<div>
														<?=$item['add_on_name'] . " @ <strong>$defaultCurrencySymbol " . number_format($item['price'], 2) . "</strong>"?><br>
													</div>
													<!-- 
													<div class="pull-right">Ref: <?=@$item['ref']?></div>
													<div>
														<?=$item['description']?>
													</div>
													 -->
												</li>
											<?php endforeach;?>																	            								
            							</ul>
            							<?php endif;?>
            						</div>    
	                    			<?php endforeach;?>
	                    			<?php else:?>
	                    			<div class="callout callout-warning">
                						<p>This menu has no Add-ons</p>
									</div>
	                    			<?php endif;?>									
	                  			</div>
	                		</div>	              		
			                
			                <div class="panel box box-primary">
								<div class="box-header with-border">
	                    			<h4 class="box-title">
	                      				<a data-toggle="collapse" data-parent="#accordion_<?=$counter?>" href="#collapseThree_<?=$counter?>" aria-expanded="false" class="collapsed">
	                        				Descriptions
	                      				</a>
	                    			</h4>
	                  			</div>                  
	                  			<div id="collapseThree_<?=$counter?>" class="panel-collapse " aria-expanded="false" >
	                    			<div class="box-body">
										<!-- <p><strong>Long Description: </strong> <br><?//=$menus->description_long?></p> <hr>-->
										<p><?=$menus->description_short?></p>
	                    			</div>
	                  			</div>
	                		</div>
	                		
	              		</div>
	            	</div>
				</div>
			</div>

		</div>
	</div>
<?php $strHtml = ob_get_clean()?>
<?php
	if($counter === 1) echo '<div class="row">';
	echo $strHtml;		
	if (!($counter % (12/$intDivGrid) )) echo '</div><div class="row">';
?>
<?php endforeach;echo '</div>';?>
<?php endif;?>