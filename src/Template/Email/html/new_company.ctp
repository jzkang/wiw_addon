

      <img src="<?=$defaultImgProfile?>"><br><br>
		<p>
		Hi <?=$strSellerName?>,
		<br><br>
		On behalf of <?=$defaultAppName?> team, we would like to thank you for your interest in joining us. Let us create a great partnership and prosper together.   
		<br><br>
		You can find your temporary password below. Remember that you are required to change this once you login to <a href="<?=$env?>"><strong><?=$defaultAppName?></strong></a>:
		<br><br>
		------<br>
		<?=$strTempPass?>
		<br>-----
		<br><br>
		Thank YOU!
		<br><br><br>
		
		<?=$defaultAppName?> Team - <?=$defaultTagLine?>
		</p>