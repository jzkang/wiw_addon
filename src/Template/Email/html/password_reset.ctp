<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    <title>Foundation for Sites</title>-->
        <link rel="stylesheet" href="../css/foundation.min.css">
        <link rel="stylesheet" href="../css/app.css">    
    </head>
    <body>

      <img src="<?=$defaultImgProfile?>"><br><br>
		<p>
		Hi <?=$strSellerName?>,
		<br><br>
		We have received a request to reset your password.
		<br><br>
		Below is your temporary password. You're required to change this when you login to <a href="<?=$env?>"><strong>foodtri</strong>PH</a>:
		<br><br>
		------<br>
		<?=$strTempPass?>
		<br>-----
		<br><br>
		Thank you and may you have an awesome day.
		<br><br><br>
		<?=$defaultAppName?> Team - <?=$defaultTagLine?>
		</p>
	</body>
</html>