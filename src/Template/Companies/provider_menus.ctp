<?php 
$this->assign('title', $vendor->name . ' - ' . $arrData->name);
$this->assign('description', $arrData->description_short);
?>
<div class="row">
	
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
	<section class="content">

        	<div class="col-md-6">
        		<div class="row">
        			<div class="col-md-12">
        				<!-- Seller Address -->
						<?= $this->element('public_vendor_menu_item', compact('arrData') );?>		
        			</div>
        		</div>       		
	        </div>
        	        
		</div>
	</section>
</div>
	