<?php
$this->assign('title', h($company->company_name));
$this->assign('sub_title', h($company->uuid));

use Cake\Routing\Router;
extract(Router::parseNamedParams($this->request)->params['named']);
if (empty($refTab)) $refTab = 'Menus';

?>

<div class="row">
	
    <!-- Content Header (Page header) -->
    
	    <!-- Main content -->
		<section class="content">
	
			<div class="row">
				<div class="col-md-12">
					<?= $this->element('company_profile');?>
	        	</div>	        	        
	      </div>
	      <!-- /.row -->

			<div class="row">
				<div class="col-md-12">
					<?php if( empty($company->latitude) || empty($company->longitude) ):?>
						<?= $this->element('frm_company_address');?>
					<?php else:?>
						<?= $this->element('company_address', compact('company') );?>
					<?php endif;?>
	        	</div>	        	        
			</div>
	      <!-- /.row -->	      
	      
	    </section>
	    <!-- /.content -->
</div>
<?= $this->element('modals',  ['id'=>'modalNewBranch','modalTitle'=>'Add New Branch','size'=>'modal-lg'])?>
<?= $this->element('modals',  ['id'=>'modalNewMenu','modalTitle'=>'Add New Menu','size'=>'modal-lg'])?>
<?= $this->element('modals',  ['id'=>'modalNewMenuAddOn','modalTitle'=>'New Menu Add-ons','size'=>'modal-lg'])?>
<?= $this->element('modals',  ['id'=>'modalNewCrew','modalTitle'=>'Add New Crew','size'=>'modal-lg'])?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1185406864918759";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>