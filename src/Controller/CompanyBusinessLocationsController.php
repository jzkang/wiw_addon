<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CompanyBusinessLocations Controller
 *
 * @property \App\Model\Table\CompanyBusinessLocationsTable $CompanyBusinessLocations
 */
class CompanyBusinessLocationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies']
        ];
        $companyBusinessLocations = $this->paginate($this->CompanyBusinessLocations);

        $this->set(compact('companyBusinessLocations'));
        $this->set('_serialize', ['companyBusinessLocations']);
    }

    /**
     * View method
     *
     * @param string|null $id Company Business Location id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $companyBusinessLocation = $this->CompanyBusinessLocations->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('companyBusinessLocation', $companyBusinessLocation);
        $this->set('_serialize', ['companyBusinessLocation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $companyBusinessLocation = $this->CompanyBusinessLocations->newEntity();
        if ($this->request->is('post')) {
            $companyBusinessLocation = $this->CompanyBusinessLocations->patchEntity($companyBusinessLocation, $this->request->data);
            if ($this->CompanyBusinessLocations->save($companyBusinessLocation)) {
                $this->Flash->success(__('The company business location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company business location could not be saved. Please, try again.'));
        }
        $companies = $this->CompanyBusinessLocations->Companies->find('list', ['limit' => 200]);
        $this->set(compact('companyBusinessLocation', 'companies'));
        $this->set('_serialize', ['companyBusinessLocation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company Business Location id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $companyBusinessLocation = $this->CompanyBusinessLocations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $companyBusinessLocation = $this->CompanyBusinessLocations->patchEntity($companyBusinessLocation, $this->request->data);
            if ($this->CompanyBusinessLocations->save($companyBusinessLocation)) {
                $this->Flash->success(__('The company business location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company business location could not be saved. Please, try again.'));
        }
        $companies = $this->CompanyBusinessLocations->Companies->find('list', ['limit' => 200]);
        $this->set(compact('companyBusinessLocation', 'companies'));
        $this->set('_serialize', ['companyBusinessLocation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Company Business Location id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $companyBusinessLocation = $this->CompanyBusinessLocations->get($id);
        if ($this->CompanyBusinessLocations->delete($companyBusinessLocation)) {
            $this->Flash->success(__('The company business location has been deleted.'));
        } else {
            $this->Flash->error(__('The company business location could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
