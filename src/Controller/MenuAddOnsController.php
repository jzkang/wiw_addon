<?php
namespace App\Controller;

use Cake\Network\Exception\NotFoundException;

use App\Controller\AppController;

/**
 * MenuAddOns Controller
 *
 * @property \App\Model\Table\MenuAddOnsTable $MenuAddOns
 */
class MenuAddOnsController extends AppController
{
// 	public function isAuthorized($user) {
	
// 		//only allow vendor users (u, su) to view except for admin users (a, sa)
// 		if ( 'u' === $user['role'] || 'su' === $user['role'] ) {
	
	
// 			if ( in_array($this->request->action, ['view','edit','delete','index','add']) ) {
					
// 				//check if user belongs to the vendor
// 				if ($user['vendor_uuid'] != $this->request->params['pass'][0]) {
// 					return $this->redirect(['controller'=>'vendorUsers', 'action' => 'logout']);
// 				}
					
// 			} //else return $this->redirect(['controller'=>'vendors', 'action' => 'view', $user['vendor_uuid']]);;
// 		}
	
	
// 		//only allow user belongs to this vendor
// 		return parent::isAuthorized($user);
// 	}	

	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($vendorUuid)
    {
    	$this->set('title','Menu Addons');
    	$menuAddOns = $this->getMenuAddOns($vendorUuid);
        $this->set(compact('vendorUuid','menuAddOns'));
        $this->set('_serialize', ['menuAddOns']);
    }

    /**
     * View method
     *
     * @param string|null $id Menu Add On id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menuAddOn = $this->MenuAddOns->get($id, [
            'contain' => ['Vendors', 'ParentMenuAddOns', 'ChildMenuAddOns']
        ]);

        $this->set('menuAddOn', $menuAddOn);
        $this->set('_serialize', ['menuAddOn']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($vendorUuid)
    {
    	$vendor = $this->MenuAddOns->Vendors->find('all')->where(['Vendors.uuid'=>$vendorUuid])->first();
		if (empty($vendor))  throw new NotFoundException(__('Vendor Not Found'));
		    	    	
        $menuAddOn = $this->MenuAddOns->newEntity();
        if ($this->request->is('post')) {
        	
        	if (!empty($this->request->data['price']))
        		$this->request->data['price'] = number_format($this->request->data['price'], 2);

        	$this->request->data['vendor_uuid'] = $vendorUuid; 
        	
            $menuAddOn = $this->MenuAddOns->patchEntity($menuAddOn, $this->request->data);
            if ($this->MenuAddOns->save($menuAddOn)) {
                $this->Flash->success(__('The menu add on has been saved.'));
                return $this->redirect($this->request->referer());
            } else {
                $this->Flash->error(__('The menu add on could not be saved. Please, try again.'));
            }
        }
        //$vendors = $this->MenuAddOns->Vendors->find('list', ['limit' => 200]);
        $this->MenuAddOns->ParentMenuAddOns->displayField('add_on_name');
        $parentMenuAddOns = $this->MenuAddOns->ParentMenuAddOns
        						->find('list', ['limit' => 200])
        						->where(['ParentMenuAddOns.vendor_id'=>$vendor->id, 'ParentMenuAddOns.parent_id'=>0, 'ParentMenuAddOns.active'=>1, 'ParentMenuAddOns.deleted'=>0]);
        
        $this->set(compact('menuAddOn', 'vendors', 'parentMenuAddOns', 'vendor'));
        $this->set('_serialize', ['menuAddOn']);
        
        if ($this->request->is('Ajax')) $this->render('add','ajax');
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu Add On id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($vendorUuid, $id)
    {
    	
    	$vendor = $this->MenuAddOns->Vendors->find('all')->where(['Vendors.uuid'=>$vendorUuid])->first();
    	if (empty($vendor))  throw new NotFoundException(__('Vendor Not Found'));    	
    	
        $menuAddOn = $this->MenuAddOns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	
            $menuAddOn = $this->MenuAddOns->patchEntity($menuAddOn, $this->request->data);
            if ($this->MenuAddOns->save($menuAddOn)) {
                $this->Flash->success(__('The menu add on has been saved.'));
                return $this->redirect($this->request->referer());
            } else {
                $this->Flash->error(__('The menu add on could not be saved. Please, try again.'));
            }
        }
        //$vendors = $this->MenuAddOns->Vendors->find('list', ['limit' => 200]);
        $parentMenuAddOns = $this->MenuAddOns->ParentMenuAddOns
        						->find('list', ['limit' => 200])
        						->where(['ParentMenuAddOns.vendor_id'=>$vendor->id, 'ParentMenuAddOns.parent_id'=>0]);
        $this->set(compact('menuAddOn', 'vendor', 'parentMenuAddOns','vendorUuid'));
        $this->set('_serialize', ['menuAddOn']);
        
        if ($this->request->is('Ajax')) $this->render('edit','ajax');
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu Add On id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function remove($id = null, $vendorUuid)
    {

        //$this->request->allowMethod(['post', 'delete']);
        $menuAddOn = $this->MenuAddOns->get($id);
        $menuAddOn->deleted = 1;
        if($menuAddOn) {
	        if ($this->MenuAddOns->save($menuAddOn)) {
	            $this->Flash->success(__('The menu add-on has been deleted successfully.'));
	            return $this->redirect($this->request->referer());
	        } else {
	            $this->Flash->error(__('The menu add-on could not be deleted. Please, try again.'));
        	}
        } else $this->Flash->error(__('The menu add-on could not be found. Please, try again.'));

        return $this->redirect($this->request->referer());
    }
}
