<?php
namespace App\Controller;

use Cake\Utility\Inflector;

use Cake\Controller\Component\AuthComponent;

use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use App\Controller\AppController;

use Cake\Utility\Hash;

use Cake\Network\Email\Email;

use Cake\Event\Event;

/**
 * CampaignNearbyPromotions Controller
 *
 * @property \App\Model\Table\CampaignNearbyPromotionsTable $CampaignNearbyPromotions
 */
class CampaignNearbyPromotionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($companyUuid)
    {
        $campaignNearbyPromotions = $this->CampaignNearbyPromotions->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);

        $grp=array();
        $list=array();
        $campaignNearbyPromotionTable = TableRegistry::get('CampaignNearbyPromotions');
        $campaignNearbyPromotion = $campaignNearbyPromotionTable->find()->where(['company_uuid'=>$companyUuid]);

        //business locations
        $list = $this->b_locations($companyUuid);

        /*
        $companyBusinessLocations = $this->b_locations($companyUuid);
        foreach ($companyBusinessLocations as $loc){
            $list[trim($loc['uuid'])]=$loc['business_name']." : ".(trim($loc["address1"])!=="" ? $loc["address1"] : "")
                                                                    .(trim($loc["address2"])!=="" ? ", ".$loc["address2"] : "")
                                                                    .(trim($loc["street"])!=="" ? ", ".$loc["street"] : "")
                                                                    .(trim($loc["state"])!=="" ? ", ".$loc["state"] : "")
                                                                    .(trim($loc["city"])!=="" ? ", ".$loc["city"] : "")
                                                                    .(trim($loc["postal_code"])!=="" ? ", ".$loc["postal_code"] : "");
        }*/

        //table row view for business groups and related locations
        $companyBusinessGroups = $this->b_groups($companyUuid);
        foreach ($companyBusinessGroups as $g){
            $tmp_para = "";
            $grp[$g['id']]['grp_name']=$g['group_name'];
            foreach (json_decode($g['business_location']) as $g_bu_loc) {
                $tmp_para .= $list[$g_bu_loc]."<br/>";
            }   
            $grp[$g['id']]['business_locations']=$tmp_para;
        }
        
//debug($grp);

        $this->set(compact('campaignNearbyPromotions','companyUuid','grp','list'));
        $this->set('_serialize', ['campaignNearbyPromotions']);
    }

    /**
     * View method
     *
     * @param string|null $id Campaign Nearby Promotion id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($companyUuid)
    {
        $campaignNearbyPromotion = $this->campaignNearbyPromotion;
        
        $this->set('campaignNearbyPromotion', $campaignNearbyPromotion);
        $this->set('_serialize', ['campaignNearbyPromotion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($companyUuid)
    {

        $action = "add";
        $user = $this->user;

        $companyBusinessGroups = $this->b_groups($companyUuid);
        $companyBusinessLocations = $this->b_locations($companyUuid);

        $campaignNearbyPromotion = $this->CampaignNearbyPromotions->newEntity();
        if ($this->request->is('post')) {

        //    die(debug($this->request->data));
            
            $campaignNearbyPromotion->company_uuid = $this->request->data['companyUuid'];

            if (isset($this->request->data['launched']) && trim($this->request->data['launched'])!=''){
                $campaignNearbyPromotion->launched="1";
                $campaignNearbyPromotion->promo_time = "";
            }else{
                $campaignNearbyPromotion->launched="0";
                $campaignNearbyPromotion->promo_time = json_encode($this->request->data['promo_time']);
            }

            $campaignNearbyPromotion->promotion_name = $this->request->data['promotion_name'];
            $campaignNearbyPromotion->description = $this->request->data['description'];
            $campaignNearbyPromotion->launch_type = $this->request->data['r1'];

            if ($this->request->data['r1']=='0'){
                $campaignNearbyPromotion->business_groups = json_encode($this->request->data['bu_group']);
                $campaignNearbyPromotion->business_locations = "";
            }
            if ($this->request->data['r1']=='1'){
                $campaignNearbyPromotion->business_groups = "";
                $campaignNearbyPromotion->business_locations = json_encode($this->request->data['bu_loc']);
            }
            

            if ($this->CampaignNearbyPromotions->save($campaignNearbyPromotion)) {
                $this->Flash->success(__('The new flash nearby promotion has been saved.'));

                return $this->redirect(['action' => 'index', $companyUuid]);
            }
            $this->Flash->error(__('The flash nearby promotion could not be saved. Please, try again.'));
        }
        
        $b_loc=array();
        
        $companyBusinessGroups = $this->b_groups($companyUuid);

        //business locations
        $b_loc = $this->b_locations($companyUuid);
    
        //debug($companyBusinessGroups);die();
        $this->set(compact('companyUuid','campaignNearbyPromotion','action','user','companyBusinessGroups','b_loc'));
        $this->set('_serialize', ['campaignNearbyPromotion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Campaign Nearby Promotion id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $companyUuid)
    {
        $action="edit";
        $campaignNearbyPromotion = $this->CampaignNearbyPromotions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //debug($this->request->data); die();

            $campaignNearbyPromotion = $this->CampaignNearbyPromotions->patchEntity($campaignNearbyPromotion, $this->request->data);

            if (isset($this->request->data['launched']) && trim($this->request->data['launched'])!=''){
                $campaignNearbyPromotion->launched="1";
                $campaignNearbyPromotion->promo_time = "";
            }else{
                $campaignNearbyPromotion->launched="0";
                $campaignNearbyPromotion->promo_time = json_encode($this->request->data['promo_time']);
            }

            $campaignNearbyPromotion->promotion_name = $this->request->data['promotion_name'];
            $campaignNearbyPromotion->description = $this->request->data['description'];
            $campaignNearbyPromotion->launch_type = $this->request->data['r1'];

            if ($this->request->data['r1']=='0'){
                $campaignNearbyPromotion->business_groups = json_encode($this->request->data['bu_group']);
                $campaignNearbyPromotion->business_locations = "";
            }
            if ($this->request->data['r1']=='1'){
                $campaignNearbyPromotion->business_groups = "";
                $campaignNearbyPromotion->business_locations = json_encode($this->request->data['bu_loc']);
            }

            if ($this->CampaignNearbyPromotions->save($campaignNearbyPromotion)) {
                $this->Flash->success(__('The campaign nearby promotion has been saved.'));

                return $this->redirect(['action' => 'index', $companyUuid]);
            }
            $this->Flash->error(__('The campaign nearby promotion could not be saved. Please, try again.'));
        }
        
        $b_loc=array();
        
        $companyBusinessGroups = $this->b_groups($companyUuid);

        $b_loc = $this->b_locations($companyUuid);
       
//debug($campaignNearbyPromotion);
        $this->set(compact('campaignNearbyPromotion','action','companyUuid','companyBusinessGroups','b_loc'));
        $this->set('_serialize', ['campaignNearbyPromotion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Campaign Nearby Promotion id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null,$companyUuid)
    {
        $this->request->allowMethod(['post', 'delete']);
        $campaignNearbyPromotion = $this->CampaignNearbyPromotions->get($id,['contain'=>[]]);
        $campaignNearbyPromotion->deleted = 1;

        if ($this->CampaignNearbyPromotions->save($campaignNearbyPromotion)) {
            $this->Flash->success(__('The campaign nearby promotion has been deleted.'));
        } else {
            $this->Flash->error(__('The campaign nearby promotion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index', $companyUuid]);
    }

    public function b_groups($companyUuid)
    {
        $companyBusinessGroupTable = TableRegistry::get('CompanyBusinessGroups');
        return $companyBusinessGroupTable->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);
    }

    public function b_locations($companyUuid)
    {
        $list=array();
        $companyBusinessLocationTable = TableRegistry::get('CompanyBusinessLocations');
        $temp=$companyBusinessLocationTable->find()->where(['company_uuid'=>$companyUuid, 'deleted'=>'0']);
        foreach ($temp as $loc){
            $list[trim($loc['uuid'])]=$loc['business_name']." : ".(trim($loc["address1"])!=="" ? $loc["address1"] : "")
                                                                    .(trim($loc["address2"])!=="" ? ", ".$loc["address2"] : "")
                                                                    .(trim($loc["street"])!=="" ? ", ".$loc["street"] : "")
                                                                    .(trim($loc["state"])!=="" ? ", ".$loc["state"] : "")
                                                                    .(trim($loc["city"])!=="" ? ", ".$loc["city"] : "")
                                                                    .(trim($loc["postal_code"])!=="" ? ", ".$loc["postal_code"] : "");
        }
        return $list;
    }
}
