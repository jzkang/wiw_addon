<?php
namespace App\Controller;

use Cake\Utility\Inflector;

use Cake\Controller\Component\AuthComponent;

use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use App\Controller\AppController;

use Cake\Utility\Hash;

use Cake\Network\Email\Email;

use Cake\Event\Event;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 */
class CompaniesController extends AppController
{

	private $company = null;

    public function beforeFilter(Event $event)
    {
    	$this->request->session()->delete('Flash');
    	
        parent::beforeFilter($event);
        $this->Auth->allow(['verifyLocation', 'provider']);
    }


	public function isAuthorized($user) {

		//only allow company users (u, su) to view except for admin users (a, sa)
		if ( 'u' === $user['role'] || 'su' === $user['role'] || 'sa' === $user['role']  ) {


			if ( in_array($this->request->action, [
					'view','edit','delete', 'profile', 'ajxEditPhoto', 'status','orders',
					'addAddress','editAddress','addOtherDetails','editOtherDetails',
			]) ) {

				/*
				//check if user belongs to the company
				if ($user['company_uuid'] != $this->request->params['pass'][0]) {
					return $this->redirect(['controller'=>'companyUsers', 'action' => 'logout']);
				}
				*/
				
				if (empty($this->company) || 'sa' === $user['role'] ) // role = sa -> always query the company details 
				{
					$strCompanyUuid = $user['company_uuid'];
					if ('sa' === $user['role']) $strCompanyUuid = $this->request->params['pass'][0];
					$this->company = $this->Companies->find('all')->where(['Companies.uuid'=>$strCompanyUuid ])
										->contain(
												[
												//'Menus' => function ($q) {return $q->select()->where(['Menus.deleted' => 0]);},
												'CompanyBusinessLocations'
										]
										)->first();
				}

			} else {
				if ('sa' !== $user['role'])
					return $this->redirect(['action' => 'view', $user['company_uuid']]);
			}
		}

		//only allow user belongs to this company
		return parent::isAuthorized($user);
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	$this->paginate = ['conditions'=>['Companies.deleted'=>0]];
        $companies = $this->paginate($this->Companies);

        $this->set(compact('companies'));
        $this->set('_serialize', ['companies']);
    }

    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($companyUuid)
    {
        $company = $this->company;

        //debug($company);

/*        
        $companyBusinessLocationTable = TableRegistry::get('CompanyBusinessLocations');
        $companyBusinessLocation = $companyBusinessLocationTable->find()->where(['company_uuid'=>$companyUuid])->first();
        debug($companyBusinessLocation);
        $this->set(compact('companyBusinessLocation'));

        //redirect user for incomplete forms
        $blnIncomplete = false;
        if (!$companyBusinessLocation->longitude && !$companyBusinessLocation->longitude) $blnIncomplete = true;
        if (empty($companyBusinessLocation->operating_hours)) $blnIncomplete = true;
        if ($blnIncomplete) return $this->redirect(['controller'=>'companies','action'=>'profile', $companyUuid]);
*/
        $this->set(compact('company','companyUuid'));
        $this->set('_serialize', ['company','companyUuid']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

    	$this->set('title', 'Create Your Company Account');

        $company = $this->Companies->newEntity();

        if ($this->request->is('post')) {

        	//get the current base on user current location
        	if (!empty($this->request->data['latlong'])) {
	        	$arrCurrency = $this->getLocationCurrency($this->request->data['latlong']);
	        	if (!empty($arrCurrency))
	        		$this->request->data['currency'] = $arrCurrency[0]['short_name'];
        	}

        	//reCaptcha - validation
			if ( ! $this->reCaptcha() ) {
				$this->Flash->error(__('reCAPTCHA missing. Please try again.'));
				//return $this->redirect($this->request->referer());
				return $this->redirect(['action'=>'add']);
			}

        	$companyUuid = $this->request->data['uuid'] = uniqid();
        	$companyEmail = $this->request->data['contact_email'];
        	$this->request->data['photo'] = Configure::read('defaultImgJson');

            $company = $this->Companies->patchEntity($company, $this->request->data);

            $errFlag = false;
            if ($this->Companies->save($company)) {

            	$tempPass = $this->generatePassword();

            	//Create CompanyUsers
            	$companyUsersTable = TableRegistry::get('CompanyUsers');
            	$companyUser = $companyUsersTable->newEntity();
            	$companyUser->uuid = uniqid('VU_');
            	$companyUser->company_uuid = $companyUuid;
            	$companyUser->email = $companyEmail;
            	$companyUser->password = $tempPass;//password_hash($tempPass, PASSWORD_BCRYPT, ['cost' => 12,]);
            	$companyUser->role = 'su';
            	$name = array_map( 'trim', explode(' ',trim($this->request->data['contact_person']),2)) ;
            	$companyUser->first_name = $name[0];
            	$companyUser->last_name = (empty($name[1])) ? '':$name[1];
            	$companyUser->mobile = $this->request->data['contact_num'];
            	if (!$companyUsersTable->save($companyUser)) $errFlag = true;
            	
            	if ($errFlag) {
            		$this->Flash->error(__('Oopppss! The was an error. Please try again.'));
            		return $this->redirect(['action' => 'add']);
            	}

            	//Send Welcome Email
				$email = new Email();
				$email->transport('mailjet');
            	$email->template('newCompany');
                $email->viewVars(['strTempPass'=>$tempPass, 'strSellerName'=>$name[0], 'env'=>Configure::read('env'), 'defaultImgProfile'=> Configure::read('defaultImgProfile')]);
            	$email->emailFormat('html');
            	$email->from(Configure::read('defaultSender'), Configure::read('defaultAppName'));
            	$email->to($companyEmail);
            	//$email->bcc(Configure::read('emailInfo'));
            	$email->subject('Welcome to ' . Configure::read('defaultAppName'));
            	$email->send();
            	//debug($email->send());die;

                $this->Flash->success(__('Please check your email for your temporary password'));
                return $this->redirect(['controller'=>'company-users', 'action' => 'login', $companyUuid]);

            } else {
                $this->Flash->error(__('The details could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('company'));
        $this->set('_serialize', ['company']);

        if ($this->request->is('Ajax'))
        	$this->render('add','ajax');
    }

    public function addAddress() {
    	 
    	$company = $this->company;    	
    	
    	if ($this->request->is(['patch', 'post', 'put'])) {

    		$company = $this->Companies->patchEntity($company, $this->request->data);
    		
    		//get the latlong of the address
    		$this->getLatLong($company);
    		
    		$company->city = strtoupper($company->city);
    		$company->state = strtoupper($company->state);
    		
    		//debug($company);die;
    		
    		if ($this->Companies->save($company)) {
    			$this->Flash->success(__('The company address has been saved.'));
    	
    			return $this->redirect($this->request->referer());
    		} else {
    			$this->Flash->error(__('The company could not be saved. Please, try again.'));
    		}
    	}
    	$this->set(compact('company'));
    	$this->set('_serialize', ['company']);
    	
    }
    
    public function editAddress ($companyUuid) {
    	
    	$company = $this->company;
    	
    	if ($this->request->is(['patch', 'post', 'put'])) {
    	
    		$company = $this->Companies->patchEntity($company, $this->request->data);
    	
    		//get the latlong of the address
    		$this->getLatLong($company);
    	
    		$company->city = strtoupper($company->city);
    		$company->state = strtoupper($company->state);
    	
    		//debug($company);die;
    	
    		if ($this->Companies->save($company)) {
    			$this->Flash->success(__('The company address has been saved.'));
    			 
    			return $this->redirect($this->request->referer());
    		} else {
    			$this->Flash->error(__('The company could not be saved. Please, try again.'));
    		}
    	}
    	$this->set(compact('company'));
    	$this->set('_serialize', ['company']);

//     	if ($this->request->is('Ajax'))
//     		$this->render($this->request->action,'ajax');    	
    	
    }
    
    public function addOtherDetails () {
    	
    	$company = $this->company;
    	 
    	if ($this->request->is(['patch', 'post', 'put'])) {
    		
    		//setup upload file
    		$arrFile = $this->uploadFile(['fieldName'=>'company_letter', 'filename'=>$company->uuid.'_Company_Letter']);
    		
    		if ($arrFile) $this->request->data['company_letter'] = json_encode($arrFile);    		
    		
    		$company = $this->Companies->patchEntity($company, $this->request->data);
    	
    		if ($this->Companies->save($company)) {
    			$this->Flash->success(__('The company has been saved.'));
    			 
    			return $this->redirect($this->request->referer());
    		} else {
    			$this->Flash->error(__('The company could not be saved. Please, try again.'));
    		}
    	}
    	$this->set(compact('company'));
    	$this->set('_serialize', ['company']);    	
    	
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($companyUuid)
    {

    	$company = $this->company;

//     	$companyAddressTable = TableRegistry::get('CompanyBusinessLocations');
//     	$companyAddress = $companyAddressTable->find()->where(['company_uuid'=>$companyUuid])->first();
//     	$this->set(compact('companyAddress'));

    	$companyUserTable = TableRegistry::get('CompanyUsers');
    	$companyUser = $companyUserTable->find()->where(['company_uuid'=>$companyUuid])->first();
    	$this->set(compact('companyUser'));

        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->data);
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been saved.'));

                return $this->redirect($this->request->referer());
            } else {
                $this->Flash->error(__('The company could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('company'));
        $this->set('_serialize', ['company']);

        if ($this->request->is('Ajax'))
        	$this->render($this->request->action,'ajax');
    }
    
    public function editOtherDetails($companyUuid)
    {
    
    	$company = $this->company;
    
    	$companyUserTable = TableRegistry::get('CompanyUsers');
    	$companyUser = $companyUserTable->find()->where(['company_uuid'=>$companyUuid])->first();
    	$this->set(compact('companyUser'));
    
    	if ($this->request->is(['patch', 'post', 'put'])) {
    		$company = $this->Companies->patchEntity($company, $this->request->data);
    		if ($this->Companies->save($company)) {
    			$this->Flash->success(__('The company has been saved.'));
    
    			return $this->redirect($this->request->referer());
    		} else {
    			$this->Flash->error(__('The company could not be saved. Please, try again.'));
    		}
    	}
    	$this->set(compact('company'));
    	$this->set('_serialize', ['company']);
    
    	if ($this->request->is('Ajax'))
    		$this->render($this->request->action,'ajax');
    }    
    

    /**
     * Delete method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($companyUuid)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->company;
        $company->deleted = 1;

        if ($this->Companies->save($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function ajxEditPhoto($companyUuid){

    	$company = $this->company;

    	if ($this->request->is(['patch', 'post', 'put'])) {

    		//setup upload file
    		$arrImg = $this->uploadImg(['filename'=>$company->uuid]);

    		if (!empty($arrImg)) $requestData['logo'] = json_encode($arrImg);
    		
    		$company = $this->Companies->patchEntity($company, $requestData);

    		if ($this->Companies->save($company)) {
    			$this->Flash->success(__('The photo was uploaded successfully.'));
    			return $this->redirect($this->request->referer());
    		} else {
    			$this->Flash->error(__('The photo could not be saved. Please, try again.'));
    		}

    	}
    	$this->set(compact('company'));
    	$this->set('_serialize', ['company']);

    	$this->render('ajx_edit_photo','ajax');

    }

    public function profile ($companyUuid) {
    	
    	$this->set(compact('companyUuid'));
    	$this->set('title', 'Store Profile');

    	$company = $this->company;

    	$this->set('company', $company);
    	$this->set('_serialize', ['company']);

    	$blnIncomplete = false;
    	$companyAddressTable = TableRegistry::get('CompanyBusinessLocation');
    	$companyAddress = $companyAddressTable->find()->where(['company_uuid'=>$companyUuid])->first();
    	$intCounter = 0;

		$strErrMsg = "";
    	if (!$companyAddress->longitude && !$companyAddress->longitude)  {
    		$blnIncomplete = true;
    		//$this->Flash->error(__(++$intCounter . ') We need to complete your address before we can activate your store.'));
			$strErrMsg .= '<ol>' . ++$intCounter . ') Invalid Address. We need to have a valid <u>Store Address</u> before we can activate your store.</ol>';
    		$this->set('missingAddress', true);
    	}
    	if (empty($companyAddress->operating_hours)) {
    		$blnIncomplete = true;
    		//$this->Flash->error(__(++$intCounter . ') We need to add Meetup Location before we can activate your store.'));
			$strErrMsg .= '<ol>'. ++$intCounter . ') We need to add <u>Meetup Location</u> before we can activate your store.</ol>';
    		$this->set('missingMeetup', true);
    	}

    	
    	$this->set(compact('companyAddress','blnIncomplete'));    	
    	
		if ($blnIncomplete) {
			
			$this->Flash->success("<h3>It's time to setup our store.</h3>", ['escape' => false]);
			$this->render('setup');
			
		} 

    }

    public function status($companyUuid){
    	//debug($this->request->data['status']);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	$company = $this->company;
            $company = $this->Companies->patchEntity($company, $this->request->data);
            $blnResult = $this->Companies->save($company);


            //set also the company_addresses.active
            if ($blnResult) {
            	$companyAddressTable = TableRegistry::get('CompanyBusinessLocation');
            	$companyAddress = $companyAddressTable->find()->where(['company_id'=>$company->id])->first();
            	$companyAddress->active = $this->request->data['status'];
            	$blnResult = $companyAddressTable->save($companyAddress);
            }

            if ($blnResult)
            	echo $this->request->data['status'];

            die;
        }
    }

    public function orders($companyUuid) {

    	$company = $this->company;

    	$arrTransactions = $this->getBranchOrders($company->company_addresses[0]->uuid);
    	$this->set('arrTransactions', $arrTransactions);

    	//$transacItems = Hash::combine($transacItems, '{n}.transaction_id', '{n}');
    	$this->set(compact('transacItems'));

    	$this->set('_serialize', ['transacItems']);
    }

	public function verifyLocation($latlong) {

		$counrty = $this->getLocationCountry($latlong);
   		if ( !in_array($counrty, Configure::read('Country')) ) {
   			if ($this->request->is('ajax')) {
   				echo "<font color='red'><br><strong>".strtoupper($counrty)."</strong> is not yet support</font><script>$('form').remove();</script>";
   			} else return true;
   		} else {
   			if ($this->request->is('ajax')) {
   				echo strtoupper($counrty);
   			} else return false;
   		}

   		die;
   }

    /**
     * Provider method - public page of the seller
     *
     */
	public function provider ($provider, $strController = null, $intId = null) {
		
		$arrProvider = explode('-',$provider);
		$companyUuid = $arrProvider[count($arrProvider)-1];

		$this->set(compact('companyUuid'));
		$this->set('title', 'Company Profile');
		
		if (empty($this->company))
			$company = $this->Companies->find('all')->where(['Companies.uuid'=>$companyUuid])
								->contain(['CompanyBusinessLocation'])
								->first();
		
		if (empty($company)) die(__LINE__);//$this->redirect('https://www.foodtri.ph');;
		
		
		$this->set('company', $company);
		$this->set('_serialize', ['company']);
		
		$companyAddressTable = TableRegistry::get('CompanyBusinessLocation');
		$companyAddress = $companyAddressTable->find()->where(['company_uuid'=>$companyUuid])->first();
		$intCounter = 0;

		$blnIncomplete = null;
		
		$arrCompanyMenus = $this->getMenus($companyUuid);
				
		$this->set(compact('provider', 'companyAddress','blnIncomplete','arrCompanyMenus'));		

		
		if (!empty($strController)) {

			$arrItem = explode('-',$intId);
			$arrItemId = $arrItem[count($arrItem)-1];
			
			$objTable = TableRegistry::get(Inflector::camelize($strController));
			$arrData{ucfirst($strController)} = $objTable->find()->where(['id'=>$arrItemId, 'company_uuid'=>$companyUuid])->first();

			//if not found, return to seller main page
			if (empty($arrData{ucfirst($strController)})) return $this->redirect([ 'action' => 'provider', $companyUuid]);
			
			$this->set("arrData", $arrData{ucfirst($strController)});
			$this->set(compact('strController'));
			
			return $this->render('provider_'.$strController,'provider');
				
		}		

		$this->render('provider','provider');
	}

	
}
