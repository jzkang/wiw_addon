<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Log\Log;

use Cake\Network\Email\Email;

use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

/**
 * Transactions Controller
 *
 * @property \App\Model\Table\TransactionsTable $Transactions */
class TransactionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Vendors', 'Users', 'Addresses', 'DeliveryMen']
        ];
        $transactions = $this->paginate($this->Transactions);

        $this->set(compact('transactions'));
        $this->set('_serialize', ['transactions']);
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => ['Vendors', 'Users', 'Addresses', 'DeliveryMen', 'TransactionItems', 'TransactionMessages']
        ]);

        $this->set('transaction', $transaction);
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transaction = $this->Transactions->newEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->data);
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
            }
        }
        $vendors = $this->Transactions->Vendors->find('list', ['limit' => 200]);
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $addresses = $this->Transactions->Addresses->find('list', ['limit' => 200]);
        $deliveryMen = $this->Transactions->DeliveryMen->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'vendors', 'users', 'addresses', 'deliveryMen'));
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->data);
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
            }
        }
        $vendors = $this->Transactions->Vendors->find('list', ['limit' => 200]);
        $users = $this->Transactions->Users->find('list', ['limit' => 200]);
        $addresses = $this->Transactions->Addresses->find('list', ['limit' => 200]);
        $deliveryMen = $this->Transactions->DeliveryMen->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'vendors', 'users', 'addresses', 'deliveryMen'));
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function status(){
    	
        if ($this->request->is(['patch', 'post', 'put'])) {
        	
        	$transac = $this->Transactions->find('all')->contain(['TransactionItems' => ['Menus' => function ($q) { return $q->select('photo');}]])
        												->where(['uuid'=>$this->request->data['uuid']])->first();                	
			
        	//Vendor Details
        	$vendorTable = TableRegistry::get('Vendors');
        	$vendor = $vendorTable->find()->select(['name','email','contact_num','currency'])->where(['id'=>$transac->vendor_id])->first();
        	
        	//User Details
        	$userTable = TableRegistry::get('Users');
        	$user = $userTable->find()->select(['first_name','email','mobile'])->where(['id'=>$transac->user_id])->first();        	
        	
            //$transac = $this->Transactions->patchEntity($transac, $this->request->data);
        	$transac->status = $this->request->data['status'];
        	if (!empty($transac->transac_history))
        		$transac->transac_history = json_decode($transac->transac_history, true);
        	$transac->transac_history[] = ["status"=>$this->request->data['status'],"datetime"=>date('Y-m-d h:i:s'),"remarks"=>@$this->request->data['reason']];
        	$transac->transac_history = json_encode($transac->transac_history);
        	$blnResult = $this->Transactions->save($transac);
        	//Log::write('debug', $transac->transac_history);
            
			if ($blnResult) {
				//Send Welcome Email
				$email = new Email();
				$email->transport('mailjet');
				$email->template('orderUpdate');
				$email->viewVars([
						'strFirstNameUser' => $user->first_name, 
						'strUserEmail' => $user->email,
						'strUserContactNum' => $user->mobile,                                                
						'strVendorName' => $vendor->name,
						'strVendorEmail' => $vendor->email,
						'strVendorContactNum' => $vendor->contact_num,
						'strOrderStatus' => Configure::read('TransacStatus')[$transac->status],
						'strPaymentMethod' => strtoupper($transac->payment_method),
						'strRefId' => $transac->uuid,
						'strTransacDate' => date('d/M/Y h:ia', strtotime($transac->created)),
						'strSubTotal' => number_format($transac->sub_total,2),
                        'arrItems' => $transac->transaction_items,
						'strCurrency' => $vendor->currency,
						'strReason' =>  @$this->request->data['reason'],
						'strMeetup' => $transac->meetup,
						'strRemarks' => $transac->remarks,
						'defaultImgProfile'=> Configure::read('defaultImgProfile'),
				]);
				$email->emailFormat('html');
				$email->from(Configure::read('emailDefault'), 'foodtriPH');
				$email->to($user->email);
				$email->cc($vendor->email);
				$email->bcc(Configure::read('systemEmail'));
				$email->subject('['.$vendor->name.'] '.Configure::read('TransacStatus')[$transac->status].' - Order#: '.$transac->uuid);
                
				$email->send();
				//debug($email->send());die;
			}
            
            echo $blnResult;die;

        }
    }
}
