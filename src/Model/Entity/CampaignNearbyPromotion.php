<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CampaignNearbyPromotion Entity
 *
 * @property int $id
 * @property string $company_uuid
 * @property string $promotion_name
 * @property string $description
 * @property string $business_groups
 * @property string $business_locations
 * @property string $launch_type
 * @property string $launched
 * @property string $promo_time
 * @property string $active
 * @property string $deleted
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class CampaignNearbyPromotion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
